<?php

return [
  'enabled' => env('FIREWALL_ENABLED', true),
  'whitelist' => [
    'ip'    =>      [
      '127.0.0.1',
      '62.80.164.25', // Developers and designers (office ip)
      '37.235.63.47', // System administrator
      '159.69.163.245', // Content managers
      '116.202.47.90', // Account managers
      '78.140.143.236' // Project managers
    ],
    'country' => []
  ],
  'blacklist' => [
    'ip'  => [],
    'country' => [

    ],
  ],
];
