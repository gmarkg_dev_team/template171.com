<?php

return [
    'routes'          => [
        'excerpt' => [
            'documents',
            'account-types',
            'contact-info',
            //
        ],
        /**
         * Redirect All
         * $allowedRouteNames
         */
        'allowed_route_names' => [
            'home',
        ],
        'voyager_menu' => [
            'page.one',
        ]
    ],
    'controllers'     => [
        'namespace' => 'EugeneShae\\Engine\\Http\\Controllers',
    ],
    'models'          => [
        'namespace' => 'EugeneShae\\Engine\\Http\\Models',
    ],
    'services'        => [
        'ipdata'     => [
            'host'    => env('IPDATA_HOST', 'https://api.ipdata.co/'),
            'api_key' => env('IPDATA_KEY', 'ca932eed25a898831d20272cda51a7aa43b92409a41c317d22d1e501'),
        ],
        'popcorn'    => [
            'host'                => env('POPCORN_HOST', null),
            'routes'              => [
                'lead' => [
                    'create' => 'lead',
                ],
            ],
        ],
        'trackbox'   => [
            'host'    => env('TRACKBOX_HOST', null),
            'api_key' => env('TRACKBOX_KEY', null),
            'auth'    => [
                'username' => env('TRACKBOX_USER', null),
                'password' => env('TRACKBOX_PASSWORD', null),
            ],
            'routes'  => [
                'lead' => [
                    'send' => 'signup/procform',
                ],
            ],
        ],
        'backoffice' => [
            'host'         => env('BACKOFFICE_HOST', null),
            'demo_host'    => env('BACKOFFICE_DEMO_HOST', null),
            'security_key' => env('BACKOFFICE_KEY', 'jWtwTqHFp3Fn8sBp'),
            'source'       => getenv('APP_NAME'),
            'routes'       => [
                'login'     => 'Account/login',
                'demo-lead' => [
                    'create' => 'Account/DemoRegister',
                ],
            ],
        ],
        'widget'     => [
        'host'      => env('WIDGET_TRADING_HOST', null),
        'auth_key'  => env('WIDGET_TRADING_AUTH_KEY', 'TJHrdoAT3v'),
      ],
    ],
    'cloudflare' => [
        'url'           => env('CLOUDFLARE_ENDPOINT', 'https://api.cloudflare.com/client/v4/zones/'),
        'zone_id'       => env('CLOUDFLARE_ZONE_ID', 'null'),
        'token'         => env('CLOUDFLARE_TOKEN', '0Ghffs_DeMm_krLKYrjES1IU2Xt_yaCGXCWe7Uk5'),
        'purge_cache'   => '/purge_cache',
    ],
    'pagination'      => [
        'news' => [
            'home' => 3,
            'all'  => 6,
        ],
        'promotions' => [
            'all'  => 6,
        ],
    ],
    'menu' => [
      'item_trading'  => env('MENU_ITEM_TRADING', 25),
    ],

    'doc' => [
      'url' => env('DOCUMENTS_URL', 'documents'),
    ],

    /*
       * News manager config
       */
    'role' => [
      'news' => [
        'user'  => 'news',
        'redirect_to' => '/admin/news'
      ]
    ],


    /**
     * Path to the Engine Assets
     * Here you can specify the location of the engine assets path
     */
    'assets_path'     => '/vendor/engine/',
    /**
     * Path to the Favicon
     * For create favicon use https://realfavicongenerator.net/
     */
    'favicon_path'    => '',

    /**
     * Here you can specify additional assets
     * you would like to be included in the layouts.index.blade
     */
    'additional_css'  => [
        //'css/custom.css',
    ],
    'additional_js'   => [
        //'js/custom.js',
    ],
    'additional_font' => [
        'https://fonts.googleapis.com/css?family=Raleway:300,400,600',
        // By default
    ],
];
