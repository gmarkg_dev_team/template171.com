<?php


namespace App\Models;

use EugeneShae\Engine\Models\Page as PageEngine;

class Page extends PageEngine
{
  protected $translatable = [
    'title',
    'slug',
    'content',
    'image_alt',
    'seo_title',
    'seo_description',
    'short_description',
  ];
}