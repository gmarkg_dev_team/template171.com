<?php

namespace App\Http\Controllers;

use EugeneShae\Engine\Http\Controllers\Controller as EngineController;

class Controller extends EngineController
{
    /**
     * Controller constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }
}
