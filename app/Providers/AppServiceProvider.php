<?php

namespace App\Providers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\Vendor\VoyagerController;
use EugeneShae\Engine\FormFields\TemplateFormField;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\MenuController;
use TCG\Voyager\Http\Controllers\VoyagerMenuController;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
      Voyager::addFormField(TemplateFormField::class);

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

      $loader = \Illuminate\Foundation\AliasLoader::getInstance();
      $loader->alias('TCG\Voyager\Http\Controllers\VoyagerController', 'App\Http\Controllers\Vendor\VoyagerController');
      $loader->alias('Barryvdh\TranslationManager\Controller', 'App\Http\Controllers\Vendor\TranslationManager\Controller');
      $loader->alias('Barryvdh\TranslationManager\Manager', 'App\Vendor\TranslationManager\Manager');

      $loader->alias('TCG\Voyager\Http\Controllers\VoyagerMenuController', 'App\Http\Controllers\Vendor\Voyager\MenuController');
    }
}
