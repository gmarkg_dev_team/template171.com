<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use EugeneShae\Engine\Models\MenuItem;
use EugeneShae\Engine\Models\Page;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('components.trading-instruments', function($view) {
            $view->with(['pages' => $this->fetchPages(MenuItem::HEADER_MENU)]);
        });
    }

    /**
     * @param int $menuId
     * @return array
     */
    private function fetchPages($menuId)
    {
        $menuItemTrading = config('engine.menu.item_trading');
        $node = MenuItem::where('id', '=', $menuItemTrading)
            ->where('menu_id', '=', $menuId)
            ->first();

        $items = (!is_null($node)) ? $node->childrenByMenu($menuId)->get() : [];
        $pagesArray = [];

        foreach ($items as $menuItem) {
            if ($menuItem->route == 'page.one') {
                $pageSlug = $menuItem->parameters->page;
                $pageOrder = $menuItem->order;
                $pagesArray[$pageOrder] = $pageSlug;
            }
        }

        ksort($pagesArray);

        $pages = Page::whereIn('slug', $pagesArray)->get();
        $sortedPages = [];

        foreach ($pagesArray as $slug) {
            foreach ($pages as $page) {
                if ($page->slug == $slug) {
                    $sortedPages[] = $page;
                    break;
                }
            }
        }

        return $sortedPages;
    }
}