<?php namespace App;

class DomainApplication extends \Illuminate\Foundation\Application
{
  /**
   * Get the another path to the language files.
   *
   * @return string
   */
  public function langPath()
  {
    return $this->resourcePath().DIRECTORY_SEPARATOR.'ltm_lang';
  }

}