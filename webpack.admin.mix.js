const mix = require('laravel-mix');

require('laravel-mix-polyfill');
require('laravel-mix-merge-manifest');

mix
  .sass('resources/assets/admin/styles/page-builder.scss', 'public/vendor/admin/css/page-builder.css')
  .js('resources/assets/admin/js/index.js', 'public/vendor/admin/js/index.js')
  .copy('resources/assets/admin/js/voyager.js', 'public/vendor/admin/js/voyager.js')
  .webpackConfig({
    externals: {
      jquery: 'jQuery',
    },
  })
  .polyfill({
    enabled: true,
    useBuiltIns: "usage",
    targets: "firefox 50, IE 11"
  })
  .sourceMaps()
  .mergeManifest()

if (mix.inProduction()) {
  mix.disableNotifications()
}
