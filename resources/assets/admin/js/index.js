function tinymcePrepareConfig(config) {
  return {
    ...config,
    image_caption: true,
    image_title: true,
    image_description: true,
    image_dimensions: false,
    image_class_list: [
      {title: 'None', value: ''},
    ],
    link_class_list: [
      {title: 'None', value: ''},
      {title: 'Button', value: 'button'},
    ]
  };
}

window.tinymcePrepareConfig = tinymcePrepareConfig;
