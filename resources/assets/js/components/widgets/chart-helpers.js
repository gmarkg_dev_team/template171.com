//Extend Chart.js

//Moving vertical line when hovering over the chart

//You would also need to:
//Set type: LineWithLine for chart type;

//Set options:
//intersect: false for tooltips;
//set configLineWithLine; (optional)

Chart.defaults.LineWithLine = Chart.defaults.line

Chart.controllers.LineWithLine = Chart.controllers.line.extend({
  draw: function(ease) {
    if (this.chart.tooltip._active && this.chart.tooltip._active.length) {
      const configLineWithLine = this.chart.config.options.configLineWithLine || {}

      let activePoint = this.chart.tooltip._active[0],
          ctx = this.chart.ctx,
          x = activePoint.tooltipPosition().x,
          topY = this.chart.legend.bottom,
          bottomY = this.chart.chartArea.bottom

      // draw line
      ctx.save()
      ctx.beginPath()
      ctx.moveTo(x, topY)
      ctx.lineTo(x, bottomY)
      ctx.lineWidth = configLineWithLine.lineWidth ||2
      ctx.strokeStyle = configLineWithLine.strokeStyle || '#07C'
      ctx.stroke()
      ctx.restore()
    }

    Chart.controllers.line.prototype.draw.call(this, ease)
  }
})