import { CryptoQuotes } from '../quotes'

const cryptoWidgetsInstances = []

export function createCryptoWidgets() {
  const nodes = document.getElementsByClassName('js-crypto-widget')

  //init widget
  CryptoQuotes.forEachArr(nodes, node => {
    const options = {
      quoteName: node.getAttribute('data-key').toUpperCase(),
      dataset: {
        fill: false,
        pointRadius: 0,
        pointBorderWidth: 0,
        pointHoverRadius: 3,
        pointHoverBorderWidth: 8
      },
      options: {
        configLineWithLine: {
          strokeStyle: 'rgba(236,236,236,0.8)',
        },
      },
    }

    if (node.getAttribute('data-common-options')) {
      options.common = JSON.parse(node.getAttribute('data-common-options'))
    }

    if (node.getAttribute('data-chart-options')) {
      options.dataset = Object.assign(
        {},
        options.dataset,
        JSON.parse(node.getAttribute('data-chart-options')))
    }

    const instance = new CryptoQuotes(node, options)

    cryptoWidgetsInstances.push(instance)
  })

  //init slider
  initCryptoSlider( $('.js-crypto-slider') )
}

export function initCryptoSlider($node) {
  if (!$node.length) return

  $node.slick({
    infinite: true,
    speed: 1500,
    dots: true,
    arrows: false,
    slidesToShow: 5,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1150,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 940,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 680,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 568,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      },
    ]
  })
}

export function updateCryptoWidgets(methodName, data) {
  CryptoQuotes.forEachArr(cryptoWidgetsInstances, instance => instance[methodName] && instance[methodName](data))
}