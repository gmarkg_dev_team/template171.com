const dataset = {
  borderWidth: 2,
  borderColor: 'black',
  backgroundColor: 'orange',
  pointRadius: 0,
}

const options = {
  responsive: true,
  maintainAspectRatio: false,

  title: {
    display: false,
    intersect: false,
  },

  legend: {
    display: false
  },

  tooltips: {
    enabled: false,
    intersect: false
  },

  scales: {
    xAxes: [{
      display: false,
      gridLines: {
        display: false
      },
      scaleLabel: {
        display: false,
      },
      ticks: {
        fontSize: 0
      },
    }],
    yAxes: [{
      display: false,
      gridLines: {
        display: false
      },
      scaleLabel: {
        display: false,
      },
      ticks: {
        fontSize: 0
      },
    }]
  },
}

const ChartConfig = {
  dataset: dataset,
  options: options,
}

export default ChartConfig