require('./menu')
const burger = '.js-burger'
const nav = '.js-nav'
const elements = burger + ', ' + nav

$(burger).on('click', function (e) {
  e.preventDefault(e)
  $(burger).hasClass('js-active') ? $(elements).removeClass('js-active') : $(elements).addClass('js-active')
})

$(document).mouseup(function (e) {
  const div = $(nav)
  if (!div.is(e.target) && div.has(e.target).length === 0 && !$(burger).is(e.target)) {
    $(elements).removeClass('js-active')
  }
})
