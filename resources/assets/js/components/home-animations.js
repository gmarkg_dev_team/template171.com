function animateCounter($el) {
  if (typeof $el === 'undefined' || !$el.length) return

  let start = $el.data('start') || 0
  let end = $el.data('end') || 100
  let diff = Math.abs(end - start)
  let duration = $el.data('duration') || 1000
  let size = start.toString().split('.')[1] ? start.toString().split('.')[1].length : 0 // 10.00 - 0.00, size of numbers after float point

  $el.prop('Counter', start).animate({
    Counter: start < end ? '+=' + diff : '-=' + diff //+=10 or -=10
  }, {
    duration: duration,
    easing: 'swing',
    step: (now) => {
      $el.text( parseFloat(now).toFixed(size) )
    }
  })
}

function initAnimation() {
  //wow
  if (window.WOW) {
    let wow = new WOW(
        {
          boxClass:     'wow',      // animated element css class (default is wow)
          animateClass: 'animated', // animation css class (default is animated)
          offset:       0,          // distance to the element when triggering the animation (default is 0)
          mobile:       true,       // trigger animations on mobile devices (default is true)
          live:         true,       // act on asynchronously loaded content (default is true)
          callback:     function(box) {
            const action = box.getAttribute('data-wow-action')


            if (action && action === 'animateCounters') {
              setTimeout(() => {
                $(box).find('.js-counter').each(function (index, el) {
                  animateCounter($(el))
                })
              }, 500)
            }
          }
        }
    )

    wow.init()
  }
}

setTimeout(initAnimation, 650)