let resizer = false
const burger = document.querySelector('.js-burger')
const li = document.querySelectorAll('header nav>ul>li')
let vw = window.innerWidth

testLangOptions() //test for header lang on present options

if ( isVisibleMobileMenu() ) {
  if (!resizer) {
    clickMenu()
    resizer = true
  }
}

window.onresize = () => {
  vw = window.innerWidth
  if ( isVisibleMobileMenu() ) {
    if (!resizer) {
      clickMenu()
      resizer = true
    }
  } else {
    if (resizer) {
      forEachArr(li, function (el) {
        const ul = el.querySelector('ul')
        if (ul) {
          ul.style = ''
          ul.classList.remove('active')
          el.classList.remove('active')
          el.removeEventListener('click', toggleSubMenu)
          el.querySelector('.js-arrow').classList.remove('down')
        }
      })
    }

    resizer = false
  }
}

function clickMenu () {
  forEachArr(li, function (el) {
    const ul = el.querySelector('ul')
    if (ul) {
      el.addEventListener('click', toggleSubMenu)
    }
  })
}

function toggleSubMenu () {
  if ($(this).closest('.js-nav').hasClass('js-active')) {
    const subMenu = $(this).find('ul')
    const mainLi = $(li)

    if (subMenu.hasClass('active')) {
      $(this).removeClass('active').find('.js-arrow').removeClass('down')
      subMenu.removeClass('active')
    } else {
      mainLi.not($(this)).removeClass('active').find('.js-arrow').removeClass('down')
      mainLi.not($(this)).find('ul').removeClass('active')
      $(this).addClass('active').find('.js-arrow').addClass('down')
      subMenu.addClass('active')
    }
  }
}

function isVisibleMobileMenu() {
  return burger && isVisible(burger)
}

function forEachArr (arr, callback) {
  for (let i = 0; i < arr.length; i++) {
    callback(arr[i], arr)
  }
}

function isVisible(node) {
  //!! - converts the value to equivalent boolean value
  return !!( node.offsetWidth || node.offsetHeight || node.getClientRects().length )
}

function testLangOptions() {
  let lang = document.querySelectorAll('.header__lang')

  for (let i = 0; i < lang.length; i +=1) {
    let el = lang[i]

    if (el && el.querySelectorAll('.header__lang--list > li').length) {
      el.classList.add('is-has-options')
    }
  }
}

/*click event for lang current, that present in header menu - for mobile devices*/
$('.js-nav .header__lang.is-has-options .header__lang--current').click(function (e) {
  e.preventDefault()

  if(vw <= 1024){
    let $parent = $(this).closest('.header__lang')
    let $subMenu = $parent.find('.header__lang--list')

    $subMenu.slideToggle(500)
    $parent.toggleClass('active')
  }
})
