import scrollTo from "../modules/go-to"
const anchorActiveClass = 'active'
const anchorNode = document.querySelector('.page-anchor')

function getDocumentScrollTop() {
  return document.body.scrollTop || document.documentElement.scrollTop
}

function debounce(func, delay) {
  let inDebounce

  return function() {
    const context = this
    const args = arguments

    clearTimeout(inDebounce)
    inDebounce = setTimeout(() => func.apply(context, args), delay)
  }
}

function on (elem, event, func) {
  if (elem.addEventListener) {
    elem.addEventListener(event, func)
  } else {
    elem.attachEvent('on' + event, func)
  }
}

function toggleAnchor(node) {
  if ( getDocumentScrollTop() >= window.innerHeight ) {
    !node.classList.contains(anchorActiveClass) && node.classList.add(anchorActiveClass)
  } else {
    node.classList.contains(anchorActiveClass) && node.classList.remove(anchorActiveClass)
  }
}

if (anchorNode) {
  scrollTo(anchorNode) //init click event on anchorNode
  toggleAnchor(anchorNode) //toggle anchorNode on init

  on(window, 'scroll', debounce(() => {
    toggleAnchor(anchorNode) //toggle anchorNode on scroll event
  }, 100))
}