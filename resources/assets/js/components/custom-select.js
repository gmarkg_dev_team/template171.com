import CustomSelect from '../modules/custom-select'
let store = []
const nodes = document.getElementsByClassName('js-custom-select')

//save instances in store
for (let i = 0; i < nodes.length; i+=1) {
  let instance = new CustomSelect(nodes[i])
  store.push(instance)
}

//click on document - close select
document.addEventListener('click', e => {
  let target = e.target

  if ( !target.classList.contains('js-custom-select') && !target.closest('.js-custom-select') ) {
    for (let i = 0; i < store.length; i+=1) {
      let instance = store[i]
      instance.close()
    }
  }
})