//polyfill for ie
if (!Element.prototype.closest) {
  Element.prototype.closest = function(css) {
    let node = this

    while (node) {
      if (node.matches(css)) return node
      else node = node.parentElement
    }

    return null
  }
}
//polyfill for ie

require('./custom-select')

/*
  All variable & function you can modify
  For rewrote or added function in Class use 'super' for copy function of extends Class. Do it in FormBrand.
  For changed or added variables use variable formBrand like works with Object

  1. Variables:
    - inputsRequired - All required inputs. By default 'document.getElementsByClassName('input')'
    - inputs - All show inputs. By default 'document.getElementsByClassName('form__input')'
    - pattern - Object of inputs pattern input.name: pattern
    - form - By default document.getElementsByClassName('form')
    - selectOption - Object of options for countryList plugin.

   2. Functions
    - _createSelect() - Create country list
    - _validate(input) - Validation of one input
    - _validateInputs(inputsRequired) - Validate input in they have value onload page
    - _onchangeInputs(inputs) - Validate input on change
    - _onchangePhone(phone) - Rules for phone changes
    - _onsubmitForm(form) - Handler for form
    - _checkFormInput(form, data) - Check form inputs. By default return valid & data
    - _successAxios(response, form, curTextBtn) - Handler of success axios request
    - _errorAxios(response, form, curTextBtn) - Handler of error axios request
    - _finnalyAxios(response, form, curTextBtn) - Handler of finally axios request
*/

import Form from '../../../../vendor/gmarkg_dev_team/engine/resources/assets/engine/js/modules/form'

class FormBrand extends Form {
  _init () {
    super._init()
  }

  _checkFormInput (form, data) {
    let response = super._checkFormInput(form, data)

    //test input name edge
    let checkEdge = form.querySelector('input[name="edge"]')

    if (checkEdge) {
      if (!checkEdge.checked) {
        checkEdge.classList.add('error')
        response.valid = false
      } else {
        checkEdge.classList.remove('error')
      }
    }
    //test input name edge

    //test custom select
    let customSelect = form.querySelectorAll('.js-input[required]')

    for (let i = 0; i < customSelect.length; i += 1) {
      let el = customSelect[i]

      if (!el.value) {
        el.closest('.js-custom-select').classList.remove('valid')
        el.closest('.js-custom-select').classList.add('error')
        response.valid = false
      } else {
        el.closest('.js-custom-select').classList.remove('error')
        el.closest('.js-custom-select').classList.add('valid')
      }
    }
    //test custom select

    return response
  }
}

const formBrand = new FormBrand()
formBrand._init()
