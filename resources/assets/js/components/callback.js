import Form from '../../../../vendor/gmarkg_dev_team/engine/resources/assets/engine/js/modules/form'

class FormCallback extends Form {
  constructor () {
    super()
  }

  _init () {
    super._init()
  }

  _createSelect() {
    const selects = document.getElementsByClassName('callback-select')

    if (selects.length > 0) {
      this.constructor.forEachArr(selects, (el) => {
        countryList(el, this.selectOption)
      })
    }
  }

  _successAxios (response, form, curTextBtn) {
    const btn = form.querySelector('button')
    btn.style.display = 'none'
    this.constructor._toggleErrorMessage(form, false) //hide error message
    this.constructor._toggleSuccessMessage(form, true) //show success message
  }

  _errorAxios (error, form, curTextBtn) {
    this.constructor._toggleSuccessMessage(form, false) //hide success message
    this.constructor._toggleErrorMessage(form, true) //show error message
    super._errorAxios(error, form, curTextBtn)
  }

  static _toggleSuccessMessage (form, flag) {
    const successMessage = form.querySelector('.success')
    successMessage.innerHTML = flag ? successMessage.dataset.success : ''
    successMessage.style.display = flag ? 'block' : 'none'
  }

  static _toggleErrorMessage (form, flag) {
    const errorMessage = form.querySelector('.error')
    errorMessage.style.display = flag ? 'block' : 'none'
  }
}

let formCallback = new FormCallback()
formCallback.form = document.getElementsByClassName('form-callback')
formCallback.selectOption.flagInSelect = true
formCallback.selectOption.required = true
formCallback.selectOption.select = false
formCallback._init()

$('.form-callback').find('input[type="tel"]').attr("autocomplete", "off").addClass("input")