'use strict'

import QuotesWatcher from '../modules/quotesWatcher'

class WidgetsQuotesWatcher extends QuotesWatcher {
  constructor() {
    super()
    this.config.fetchTotalData = true
    this.config.watching = true
  }

  callback(type, response) {
    //console.log('widgets-quotes-type - ', type, ':', 'widgets-quotes-response - ', response)

    switch (type) {
      case 'getFirstData':
        break;

      case 'getTotalData':
        break;

      case 'watch':
        break;
    }
  }
}

const widgetsQuotesWatcher = new WidgetsQuotesWatcher()

document.addEventListener("DOMContentLoaded", () => {
  widgetsQuotesWatcher.init()
})