import Quotes from '../modules/quotes'
import ChartConfig from './widgets/chart-config'
typeof Chart !== 'undefined' && require('./widgets/chart-helpers')

export class SliderQuotes extends Quotes {
  constructor(node) {
    super(node)
    this.config.quotesList = ['EURUSD', 'BTCUSD', 'ETHUSD', 'GBPUSD']
  }

  updateFirst(response) {
    super.updateFirst(response)
    this.initSlider()
    super.show()
  }

  initSlider() {
    const slider = this.node.querySelector('.js-slider')
    if (!slider) return

    $(slider).slick({
      infinite: true,
      autoplay: true,
      autoplaySpeed: 1500,
      speed: 1500,
      dots: false,
      arrows: false,
      slidesToShow: 3,
      slidesToScroll: 1,
      rtl: $('body').hasClass('rtl'),
      responsive: [
        {
          breakpoint: 1150,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
          }
        },
      ]
    })
  }
}

export class TickerQuotes extends Quotes {
  constructor(node) {
    super(node)
    this.config.quotesList = ['EURUSD', 'GBPUSD', 'USDCNH', 'NAS100', 'BTCUSD', 'ETHUSD', 'LTCUSD', 'SPOT']
    this.config.fetchTotalData = true
  }

  updateTotal(response) {
    super.updateTotal(response)
    super.show()
  }

  createHtml(data, delta) {
    let html = ''
    let value = this.constructor.toFixed(data[this.config.quotesField], 5, 7)

    html += '<span class="ticker-quotes__item--bid-ask">' + value + '</span>'

    if (delta && delta.percent && delta.static) {
      let deltaPercent = this.constructor.toFixed(delta.percent, 2, 7)
      let deltaStatic = this.constructor.toFixed(delta.static, 5, 7)
      let deltaPercentStr = deltaPercent > 0 ? '+' + deltaPercent + '%' : deltaPercent + '%'
      let deltaStaticStr = deltaStatic > 0 ? '+' + deltaStatic : deltaStatic

      html += '<div class="ticker-quotes__item--bid-delta ' + delta.cls + '">'
      html += '<span>' + deltaPercentStr + '</span>'
      html += '<span>' + deltaStaticStr + '</span>'
      html += '</div>'
    }

    return html
  }
}

export class ChartQuotes extends Quotes {
  constructor(node, options = {}) {
    super(node)

    const common = {
      startIndex: parseInt(this.node.getAttribute('data-start-index')) || 0,
      autoplay: this.node.hasAttribute('data-autoplay'),
      autoplaySpeed: parseInt(this.node.getAttribute('data-autoplay-speed')) || 3000,
    }

    const props = {
      quotesList: options.quotesList || ['BTCUSD', 'EURUSD', 'NAS100', 'ETHUSD'],
      dataset: Object.assign({}, ChartConfig.dataset, options.dataset),
      options: Object.assign({}, ChartConfig.options, options.options),
      common:  Object.assign({}, common, options.common),
    }

    this.init(props)
    this.events()
  }

  updateTotal(data) {
    super.updateTotal(data)
    this.change(this.common.startIndex)
    this.common.autoplay && this.play()
    super.show()
  }

  createHtml(data, delta) {
    let html = ''
    let value = this.constructor.toFixed(data[this.config.quotesField], 5, 7)

    html += '<div><span>' + value + '</span></div>'

    if (delta && delta.percent && delta.static) {
      let deltaPercent = this.constructor.toFixed(delta.percent, 2, 7)
      let deltaStatic = this.constructor.toFixed(delta.static, 5, 7)
      let deltaPercentStr = deltaPercent > 0 ? '+' + deltaPercent + '%' : deltaPercent + '%'
      let deltaStaticStr = deltaStatic > 0 ? '+' + deltaStatic : deltaStatic

      html += '<div class="' + delta.cls + '">'
      html += '<span>' + deltaPercentStr + '</span>'
      html += '<span>' + deltaStaticStr + '</span>'
      html += '</div>'
    }

    return html
  }

  /*add new methods*/
  init(props) {
    /*overwrite super properties*/
    this.config.quotesList = props.quotesList
    this.config.fetchTotalData = true

    /*add new properties*/
    this.canvasNode = this.node.getElementsByClassName('js-canvas')
    this.itemsNode = this.node.getElementsByClassName('js-item')

    //chart state and options
    this.chart = {
      instance: null, //chart js instance
      data: [], //array of OpenPrice values of 1 day for one quoteName
      labels: [], //array of labels for chart js scales: xAxes and yAxes
      dataset: [props.dataset],
      options: props.options,
    }

    this.intervalId = null
    this.currentIndex = null

    this.common = props.common
  }

  loadChart() {
    if (!this.canvasNode) return

    let quoteName = this.config.quotesList[this.currentIndex]
    let data = [] //array of OpenPrice values of 1 day for one quoteName
    this.chart.labels = []

    this.constructor.forEachArr(this.totalData.day, (obj) => {
      if (obj.Symbol && obj.Symbol === quoteName && obj.Ticks) {
        this.constructor.forEachArr(obj.Ticks, (tickValue, tickKey) => {
          if (tickValue.OpenPrice) {
            data.push(tickValue.OpenPrice)
            this.chart.labels.push(tickKey)
          }
        })
      }
    })

    if (!this.chart.instance) this.createChart()
    this.updateChart(data)
    quoteName && this.toggleClassNameByKey(this.itemsNode, 'active', quoteName.toLowerCase())
  }

  createChart() {
    if (!this.canvasNode) return

    this.chart.instance = new Chart(this.canvasNode, {
      type: 'line',
      data: {
        labels: this.chart.labels,
        datasets: this.chart.dataset,
      },
      options: this.chart.options,
    })
  }

  updateChart(data) {
    if (!data) return

    this.chart.instance.data.labels = this.chart.labels
    this.constructor.forEachArr(this.chart.instance.data.datasets, (dataset) => {
      dataset.data = data
    })

    this.chart.instance.update()
  }

  play() {
    if (this.intervalId) return

    this.intervalId = setInterval(() => this.change(), this.common.autoplaySpeed)
  }

  pause() {
    if (!this.intervalId) return

    clearInterval(this.intervalId)
    this.intervalId = null
  }

  change(index) {
    if (!Object.keys(this.totalData.day).length) return

    if (typeof index !== 'undefined') {
      this.currentIndex = index
    } else {
      if (this.currentIndex === null) this.currentIndex = -1
      this.currentIndex += 1
    }

    if (this.currentIndex > this.config.quotesList.length - 1) this.currentIndex = 0

    this.loadChart()
  }

  toggleClassNameByKey(collections, className, key) {
    this.constructor.forEachArr(collections, (el) => {
      if (el.getAttribute('data-key') === key) {
        el.classList.add(className)
      } else {
        el.classList.remove(className)
      }
    })
  }

  events() {
    //add mouseenter, mouseleave events for pause, play animation
    this.constructor.on(this.node, 'mouseenter', () => this.common.autoplay && this.pause())
    this.constructor.on(this.node, 'mouseleave', () => this.common.autoplay && this.play())

    //add click event to items
    this.constructor.forEachArr(this.itemsNode, (el) => {
      let key = el.getAttribute('data-key')
      let index = this.config.quotesList.indexOf(key.toUpperCase())

      this.constructor.on(el, 'click', (e) => {
        e.preventDefault()
        this.change(index)
      })
    })
  }
}

export class TableQuotes extends Quotes {
  constructor(node) {
    super(node)
    this.config.quotesList = ['EURUSD', 'GBPUSD', 'USDCHF', 'USDCAD', 'USDCNH', 'USDSEK', 'EURGBP']
    this.mapping = {
      'EURUSD': 'EUR/USD',
      'GBPUSD': 'GBP/USD',
      'USDCHF': 'USD/CHF',
      'USDCAD': 'USD/CAD',
      'USDCNH': 'USD/CNH',
      'USDSEK': 'USD/SEK',
      'EURGBP': 'EUR/GBP'
    }
  }

  updateFirst(response) {
    super.updateFirst(response)
    super.show()
  }

  createHtml(data, delta) {
    let html = ''
    let valueBid = this.constructor.toFixed(data['Bid'], 4, 7)
    let valueAsk = this.constructor.toFixed(data['Ask'], 4, 7)

    html += '<td>' + this.mapping[data.Symbol] + '</td>'
    html += '<td>' + valueBid +'</td>'
    html += '<td>' + valueAsk + '</td>'

    return html
  }
}

export class TradingQuotes extends Quotes {
  constructor(node, options = {}) {
    super(node)

    const common = {
      startIndex: parseInt(this.node.getAttribute('data-start-index')) || 0,
      autoplay: this.node.hasAttribute('data-autoplay'),
      autoplaySpeed: parseInt(this.node.getAttribute('data-autoplay-speed')) || 3000,
      translates: {
        'item-price-title': this.node.getAttribute('data-item-price-title') || '',
        'item-percent-title': this.node.getAttribute('data-item-percent-title') || '',
      },
      mapping: {
        'EURUSD': 'EUR/USD',
        'EURCAD': 'EUR/CAD',
        'GBPUSD': 'GBP/USD',
        'USDSEK': 'USD/SEK',
        'GBPCAD': 'GBP/CAD',
      },
    }

    const props = {
      quotesList: options.quotesList || ['EURUSD', 'EURCAD', 'GBPUSD', 'USDSEK', 'GBPCAD'],
      dataset: Object.assign({}, ChartConfig.dataset, options.dataset),
      options: Object.assign({}, ChartConfig.options, options.options),
      common:  Object.assign({}, common, options.common),
    }

    this.init(props)
    this.events()
  }

  updateTotal(data) {
    super.updateTotal(data)
    this.change(this.common.startIndex)
    this.common.autoplay && this.play()
    super.show()
  }

  createHtml(data, delta) {
    let html = ''
    let value = this.constructor.toFixed(data[this.config.quotesField], 5, 7)

    html += '<h4 class="item--title">' + this.common.mapping[data.Symbol] + '</h4>'
    html += '<ul class="item--list">'
    html += '<li><span>' + this.common.translates['item-price-title'] + '</span><span>' + value + '</span></li>'

    if (delta && delta.percent) {
      let deltaPercent = this.constructor.toFixed(delta.percent, 2, 7)
      let deltaPercentStr = deltaPercent > 0 ? '+' + deltaPercent + '%' : deltaPercent + '%'

      html += '<li><span>' + this.common.translates['item-percent-title'] + '</span><span class="item--percent">' + deltaPercentStr + '</span></li>'
    } else {
      html += '<li></li>'
    }

    html += '</ul>'

    return html
  }

  /*add new methods*/
  init(props) {
    /*overwrite super properties*/
    this.config.quotesList = props.quotesList
    this.config.fetchTotalData = true

    /*add new properties*/
    this.canvasNode = this.node.getElementsByClassName('js-canvas')
    this.tabsNode = this.node.getElementsByClassName('js-tab-item')
    this.itemsNode = this.node.getElementsByClassName('js-currency-item')

    //chart state and options
    this.chart = {
      instance: null, //chart js instance
      data: [], //array of OpenPrice values of 1 day for one quoteName
      labels: [], //array of labels for chart js scales: xAxes and yAxes
      dataset: [props.dataset],
      options: props.options,
    }

    this.intervalId = null
    this.currentIndex = null

    this.common = props.common
  }

  loadChart() {
    if (!this.canvasNode) return

    let quoteName = this.config.quotesList[this.currentIndex]
    let data = [] //array of OpenPrice values of 1 day for one quoteName
    this.chart.labels = []

    this.constructor.forEachArr(this.totalData.day, (obj) => {
      if (obj.Symbol && obj.Symbol === quoteName && obj.Ticks) {
        this.constructor.forEachArr(obj.Ticks, (tickValue, tickKey) => {
          if (tickValue.OpenPrice) {
            data.push(tickValue.OpenPrice)
            this.chart.labels.push(tickKey)
          }
        })
      }
    })

    if (!this.chart.instance) this.createChart()
    this.updateChart(data)
    quoteName && this.toggleClassNameByKey(this.tabsNode, 'active', quoteName.toLowerCase())
    quoteName && this.toggleClassNameByKey(this.itemsNode, 'active', quoteName.toLowerCase())
  }

  createChart() {
    if (!this.canvasNode) return

    this.chart.instance = new Chart(this.canvasNode, {
      type: 'line',
      data: {
        labels: this.chart.labels,
        datasets: this.chart.dataset,
      },
      options: this.chart.options,
    })
  }

  updateChart(data) {
    if (!data) return

    this.chart.instance.data.labels = this.chart.labels
    this.constructor.forEachArr(this.chart.instance.data.datasets, (dataset) => {
      dataset.data = data
    })

    this.chart.instance.update()
  }

  play() {
    if (this.intervalId) return

    this.intervalId = setInterval(() => this.change(), this.common.autoplaySpeed)
  }

  pause() {
    if (!this.intervalId) return

    clearInterval(this.intervalId)
    this.intervalId = null
  }

  change(index) {
    if (!Object.keys(this.totalData.day).length) return

    if (typeof index !== 'undefined') {
      this.currentIndex = index
    } else {
      if (this.currentIndex === null) this.currentIndex = -1
      this.currentIndex += 1
    }

    if (this.currentIndex > this.config.quotesList.length - 1) this.currentIndex = 0

    this.loadChart()
  }

  toggleClassNameByKey(collections, className, key) {
    this.constructor.forEachArr(collections, (el) => {
      if (el.getAttribute('data-key') === key) {
        el.classList.add(className)
      } else {
        el.classList.remove(className)
      }
    })
  }

  events() {
    //add mouseenter, mouseleave events for pause, play animation
    this.constructor.on(this.node, 'mouseenter', () => this.common.autoplay && this.pause())
    this.constructor.on(this.node, 'mouseleave', () => this.common.autoplay && this.play())

    //add click event to items
    this.constructor.forEachArr(this.tabsNode, (el) => {
      let key = el.getAttribute('data-key')
      let index = this.config.quotesList.indexOf(key.toUpperCase())

      this.constructor.on(el, 'click', (e) => {
        e.preventDefault()
        this.change(index)
      })
    })
  }
}

export class CategoriesQuotes extends Quotes {
  constructor(node) {
    super(node)
    this.init()
  }

  updateFirst(response) {
    super.updateFirst(response)
    this.change(this.startIndex)
    this.autoplay && this.play()
    super.show()
  }

  createHtml(data, delta) {
    let html = ''
    let valueBid = this.constructor.toFixed(data['Bid'], 4, 6)
    let valueAsk = this.constructor.toFixed(data['Ask'], 4, 6)

    html += '<div>' + valueBid + '</div>'
    html += '<div>' + valueAsk + '</div>'

    return html
  }

  /*add new methods*/
  init() {
    /*overwrite super properties*/
    this.config.quotesList =
        ['BTCUSD', 'EURUSD', 'GBPUSD', 'USDCNH',
          'Google', 'APPL', 'Alibaba', 'TSL',
          'COFFEE', 'SUGAR', 'WHEAT', 'COTTON',
          'JPN225', 'NAS100', 'US30', 'ITA40']
    this.config.fetchTotalData = false

    /*add new properties*/
    this.categories = []
    this.chartInstance = null //chart js instance
    this.chartLabels = [] //array of labels for chart js scales: xAxes and yAxes

    this.currentIndex = null
    this.categoryButtons = this.node.getElementsByClassName('js-btn')
    this.tabCollection = this.node.getElementsByClassName('js-tab')
    this.autoplay = this.node.hasAttribute('data-autoplay')
    this.startIndex = parseInt(this.node.getAttribute('data-start-index')) || 0
    this.autoplaySpeed = parseInt(this.node.getAttribute('data-autoplay-speed')) || 3000

    this.intervalId = null

    this.constructor.forEachArr(this.categoryButtons, (node) => {
      this.categories.push(node.getAttribute('data-key'))
    })

    /*init events*/
    this.events()
  }

  play() {
    if (this.intervalId) return

    this.intervalId = setInterval(() => this.change(), this.autoplaySpeed)
  }

  pause() {
    if (!this.intervalId) return

    clearInterval(this.intervalId)
    this.intervalId = null
  }

  change(index) {
    this.setIndex(index)

    let key = this.categories[this.currentIndex]

    this.toggleClassNameByKey(this.tabCollection, 'active', key)
    this.toggleClassNameByKey(this.categoryButtons, 'active', key)
  }

  setIndex(index) {
    if (typeof index !== 'undefined') {
      this.currentIndex = index
    } else {
      if (this.currentIndex === null) {
        this.currentIndex = -1 // последний элемент
      }

      this.currentIndex += 1
    }

    if (this.currentIndex > this.categories.length - 1) this.currentIndex = 0
  }

  toggleClassNameByKey(collections, className, key) {
    this.constructor.forEachArr(collections, (el) => {
      if (el.getAttribute('data-key') === key) {
        el.classList.add(className)
      } else {
        el.classList.remove(className)
      }
    })
  }

  events() {
    //add mouseenter, mouseleave events for pause, play animation
    this.constructor.on(this.node, 'mouseenter', () => this.autoplay && this.pause())
    this.constructor.on(this.node, 'mouseleave', () => this.autoplay && this.play())

    //add click event to items
    this.constructor.forEachArr(this.categoryButtons, (el) => {
      let key = el.getAttribute('data-key')
      let index = this.categories.indexOf(key)

      this.constructor.on(el, 'click', (e) => {
        e.preventDefault()
        this.change(index)
      })
    })
  }
}

export class CryptoQuotes extends Quotes {
  constructor(node, options = {}) {
    super(node)

    const common = {
      className:      '',
      digits:         2,
      updateDelay:    2.5, //delay for update in watcher in seconds
      updateDuration: 2.5, //duration for update in watcher in seconds
    }

    const props = {
      quoteName: options.quoteName || 'BTCUSD',
      dataset: Object.assign({}, ChartConfig.dataset, options.dataset),
      options: Object.assign({}, ChartConfig.options, options.options),
      common:  Object.assign({}, common, options.common),
    }

    this.init(props)
  }

  updateFirst(data) {
    super.updateFirst(data)
    this.quoteContractSize = (this.firstData && this.firstData[this.quoteName]) ? this.firstData[this.quoteName]['ContractSize'] : 1
    this.node.classList.add(this.common.className)
  }

  updateTotal(data) {
    super.updateTotal(data)
    this.initChart()
    super.show()
  }

  updateWatch(data) {
    if (data[this.quoteName]) {
      this.watchStore = data
    }

    if (!this.watchIntervalId) {
      this.watchIntervalId = setInterval(() => {
        super.updateWatch(this.watchStore)
      }, this.common.updateDelay * 1000)
    }
  }

  createHtml(data, delta) {
    let price = this.roundPrice(data[this.config.quotesField])

    //update watch date and price
    this.updateDate('watch', data.Ctm)
    this.updatePrice('watch', price)

    //update delta price
    if (delta && delta.percent && delta.static) {
      let deltaStatic = this.roundPrice(delta.static)
      let deltaPercent = this.roundPrice(delta.percent, 2)

      this.updatePrice('delta', [deltaStatic, deltaPercent])

      //updated dynamic point
      this.chart.dynamicPoint.OpenPrice = price
      this.chart.dynamicPoint.RateTime = data.Ctm
    }

    if (this.chart.instance) {
      this.chart.data.pop() //removed dynamic point
      this.chart.data.push(this.chart.dynamicPoint) //pushed new dynamic point
      this.updateChart()
    }
  }

  /*add new methods*/
  init(props) {
    /*overwrite super properties*/
    this.config.quotesList = [props.quoteName]
    this.config.fetchTotalData = true
    this.config.updateHtml = false

    /*add new properties*/
    this.quoteName = props.quoteName
    this.quoteContractSize = 1

    this.watchStore = {} //store data from watcher update

    this.watchIntervalId = null

    //nodes
    this.dateNode = {
      watch: this.node.querySelector('.js-date-watch'),
      chart: this.node.querySelector('.js-date-chart'),
    }
    this.priceNode = {
      watch: this.node.querySelector('.js-price-watch'),
      chart: this.node.querySelector('.js-price-chart'),
      delta: this.node.querySelector('.js-price-delta'),
    }
    this.canvasNode = this.node.querySelector('.js-canvas')

    //chart state and options
    this.chart = {
      instance: null, //chart js instance
      data: [], //array of OpenPrice values of 1 day for one quoteName
      labels: [], //array of labels for chart js scales: xAxes and yAxes
      dynamicPoint: {OpenPrice: null, RateTime: null}, //point of latest watcher data
      dataset: [props.dataset],
      options: props.options,
    }

    //common options
    this.common = props.common
  }

  initChart() {
    if (!this.canvasNode) return

    const data = []
    const labels = []

    this.constructor.forEachArr(this.totalData.day, obj => {
      if (obj.Symbol && obj.Symbol === this.quoteName && obj.Ticks) {
        this.constructor.forEachArr(obj.Ticks, (tickValue, tickKey) => {
          if (tickValue.OpenPrice) {
            tickValue.OpenPrice = this.roundPrice(tickValue.OpenPrice / this.quoteContractSize)
            data.push(tickValue)
            labels.push(tickKey)
          }
        })
      }
    })

    //pushed dynamic point
    data.push(this.chart.dynamicPoint)
    labels.push(labels.length)

    //save to chart
    this.chart.data = data
    this.chart.labels = labels

    //run chart
    this.createChart()
    this.updateChart()
    this.eventsChart()
  }

  createChart() {
    this.chart.instance = new Chart(this.canvasNode, {
      type: 'LineWithLine',
      data: {
        labels: this.chart.labels,
        datasets: this.chart.dataset,
      },
      options: Object.assign(
        {},
        this.chart.options,
        {
          scales: {
            xAxes: [{
              offsetGridLines: true,
              gridLines: {
                color: 'transparent',
              },
              scaleLabel: {
                display: false,
              },
              ticks: {
                fontSize: 0
              },
              offset: true,
            }],

            yAxes: [{
              offsetGridLines: true,
              gridLines: {
                color: 'transparent',
              },
              scaleLabel: {
                display: false,
              },
              ticks: {
                mirror: true,
                fontColor: 'transparent',
              },
              offset: true,
            }]
          },
          hover: {
            mode: 'nearest',
            intersect: false,
            onHover: (e, element) => {
              element && element[0] && this.onChartHoverHandler(element[0]._index)
            }
          },
        }),
    })
  }

  updateChart() {
    const data = []

    this.constructor.forEachArr(this.chart.data, el => data.push(el.OpenPrice))

    this.chart.instance.data.datasets[0].data = data
    this.chart.instance.data.labels = this.chart.labels
    this.chart.instance.update()
  }

  updateDate(source, timestamp) {
    if (!source || !timestamp || !this.dateNode[source]) return

    const date = new Date()
    date.setTime(timestamp * 1000) //1000 - magic factor

    const localTime = new Date().setTime(date.getTime() + (this.config.timeZoneOffset * 60 * 1000) ) //add timeZoneOffset
    const localDate = new Date(localTime)

    this.dateNode[source].innerHTML = this.constructor.formatDate(localDate)
  }

  updatePrice(source, price) {
    if (!source || !price || !this.priceNode[source]) return

    switch (source) {
      case 'watch':
        this.updatePriceWatch(source, price)
        break

      case 'chart':
        this.updatePriceChart(source, price)
        break

      case 'delta':
        this.updatePriceDelta(source, price)
        break

      default:
        console.log(source, price)
        break
    }
  }

  updatePriceWatch(source, price) {
    const node = this.priceNode[source].getElementsByClassName('js-price')[0]

    if (node)
      this.animatePrice(node.textContent || 0, price, $(node))
  }

  updatePriceChart(source, price) {
    const node = this.priceNode[source].getElementsByClassName('js-price')[0]

    if (node)
      node.innerHTML = price
  }

  updatePriceDelta(source, price) {
    const staticNode = this.priceNode[source].getElementsByClassName('js-static')[0]
    const percentNode = this.priceNode[source].getElementsByClassName('js-percent')[0]

    if (staticNode && percentNode && Array.isArray(price)) {
      staticNode.innerHTML = price[0]
      percentNode.innerHTML = price[1]
    }
  }

  animatePrice(start, end, $node) {
    const diff = Math.abs(end - start)
    const size = end.toString().split('.')[1] ? end.toString().split('.')[1].length : 0 //size of numbers after float point
    const duration = this.common.updateDuration * 1000

    $node.prop('Counter', start).animate({
      Counter: start < end ? '+=' + diff : '-=' + diff //+=10 or -=10
    }, {
      duration: duration,
      easing: 'swing',
      step: function(now) {
        $node.text( parseFloat(now).toFixed(size) )
      }
    })
  }

  roundPrice(price, digits = this.common.digits) {
    return this.constructor.toFixed(price, digits, digits)
  }

  onChartHoverHandler(index) {
    if (typeof index === 'undefined') return

    const chartData = this.chart.data[index]

    if (chartData) {
      this.updateDate('chart', chartData.RateTime)
      this.updatePrice('chart', chartData.OpenPrice)
    }
  }

  eventsChart() {
    this.constructor.on(this.canvasNode, 'mouseenter', () => {
      this.constructor.forEachArr(['dateNode', 'priceNode'], key => {
        this[key] && this[key].watch && this[key].watch.classList.remove('active')
        this[key] && this[key].chart && this[key].chart.classList.add('active')
      })
    })

    this.constructor.on(this.canvasNode, 'mouseleave', () => {
      this.constructor.forEachArr(['dateNode', 'priceNode'], key => {
        this[key] && this[key].chart && this[key].chart.classList.remove('active')
        this[key] && this[key].watch && this[key].watch.classList.add('active')
      })
    })
  }

  static formatDate(date) {
    const hours = date.getHours()
    const minutes = date.getMinutes()
    const seconds = date.getSeconds()
    const time = [hours, minutes, seconds].map(el => el < 10 ? '0' + el : el)

    return `${date.getMonth() + 1}/${date.getDate()}/${date.getFullYear()} ${time[0]}:${time[1]}:${time[2]}`
  }
}

export class CorpQuotes extends Quotes {
  constructor(node) {
    super(node)
    this.config.quotesList = ['Apple', 'Amazon', 'Microsoft', 'Facebook', 'Netflix', 'Intel', 'Tesla', 'Uber', 'DROPBOX',]

    this.resizeTimer = false
    this.mediaQueryDesktopMin = "(min-width: 768px)"
    this.slider = {
      node: node.querySelector('.js-slider'),
    }
  }

  updateFirst(response) {
    super.updateFirst(response)
    super.show()
    this.init()
  }

  init() {
    if (this.slider.node) {
      this.update()
      this.events()
    }
  }

  update() {
    if (window.matchMedia(this.mediaQueryDesktopMin).matches) {
      this.destroySlider()
    } else {
      this.initSlider()
    }
  }

  initSlider() {
    if (!this.slider.node || this.slider.node.classList.contains('slick-initialized')) return

    $(this.slider.node).slick({
      infinite: true,
      dots: true,
      arrows: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: false,
      speed: 1000,
      rtl: $('body').hasClass('rtl'),
    })
  }

  destroySlider() {
    if (!this.slider.node || !this.slider.node.classList.contains('slick-initialized')) return

    $(this.slider.node).slick('unslick')
  }

  events() {
    this.constructor.on(window, 'resize', () => {
      if (this.resizeTimer) clearTimeout(this.resizeTimer)

      this.resizeTimer = setTimeout( () => {
        this.update()
      }, 250)
    })
  }
}