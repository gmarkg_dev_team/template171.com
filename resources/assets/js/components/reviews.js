function initReviewsCarousel(section) {
  if (!section) return

  const slider = section.getElementsByClassName('js-slider')[0]

  if (!slider || slider.classList.contains('slick-initialized')) return

  $(slider).slick({
    infinite: true,
    dots: true,
    arrows: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: false,
    speed: 1000,
    rtl: $('body').hasClass('rtl'),
  })
}

initReviewsCarousel(document.getElementById('js-reviews-slider'))