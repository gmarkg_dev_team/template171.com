export default class CustomSelect {
  constructor(node) {
    this.node = node
    this.header = node.querySelector('.js-header')
    this.title = node.querySelector('.js-title')
    this.options = Array.prototype.slice.call(node.getElementsByClassName('js-option'))
    this.input = node.querySelector('.js-input')
    this.opened = false

    this.init()
  }

  init() {
    this.change(this.input.value)
    this.events()
  }

  change(value) {
    let isPlaceholder = true
    let active_option = this.options[0]

    if (value) {
      active_option = this.options.filter(el => {
        return el.getAttribute('data-value') === value
      })[0]

      isPlaceholder = false
      this.node.classList.remove('error')
      this.node.classList.add('valid')
    } else {
      value = active_option.getAttribute('data-value')
      this.node.classList.remove('valid')
    }

    if (this.title && active_option) {
      this.title.innerText = active_option.getAttribute('data-title')

      if (isPlaceholder) {
        this.title.classList.add('is-placeholder')
      } else {
        this.title.classList.remove('is-placeholder')
      }
    }

    if (this.input)
      this.input.value = value

    this.close()
    this.myDispatchEvent('change')
  }

  myDispatchEvent (eventName) {
    let event = document.createEvent('Event')

    event.initEvent(eventName, true, true)
    this.node.dispatchEvent(event)
  }

  toggle() {
    this.opened ? this.close() : this.open()
  }

  open() {
    this.node.classList.add("opened")
    this.opened = true
  }

  close() {
    this.node.classList.remove("opened")
    this.opened = false
  }

  events() {
    this.constructor.on(this.header, 'click', e => {
      e.preventDefault()
      this.toggle()
    })

    this.constructor.forEachArr(this.options, el => {
      this.constructor.on(el, 'click', e => {
        e.preventDefault()
        this.change(e.target.getAttribute('data-value'))
      })
    })
  }

  static forEachArr (arr, callback) {
    for (let i = 0; i < arr.length; i+=1) {
      callback(arr[i], arr, i)
    }
  }

  static on (elem, event, func) {
    if (elem.addEventListener) {
      elem.addEventListener(event, func)
    } else {
      elem.attachEvent('on' + event, func)
    }
  }
}