export default class Quotes {
  constructor(node) {
    this.node = node
    this.store = {}
    this.firstData = {}
    this.totalData = {
      'month': [],
      'day': [],
    }
    this.config = {
      'quotesList':     [], //array of quotes in uppercase, if empty - get all quotes
      'quotesField':    'Bid', //Bid || Ask
      'clsArray':       ['up', 'down'], //class names for current quote value
      'fetchTotalData': false, //calculate deltaData by totalData
      'updateHtml': true, //updateHtml on changeDom event
      'timeZoneOffset': -120 //time zone offset in minutes on api server https://mt.theforexgo.com
    }
  }

  /**
   *
   * @param {object} quote
   * @param {string} quoteName
   * @returns {object}
   */
  getDeltaData(quote, quoteName) {
    let quoteValue = quote[this.config.quotesField]
    let delta = {
      'static': null,
      'percent': null,
      'cls': null //class name
    }

    return this.config.fetchTotalData
      ? this.getDeltaDataByTotalData(quoteName, quoteValue, delta)
      : this.getDeltaDataByStore(quoteName, quoteValue, delta)
  }

  /**
   *
   * @param {string} quoteName
   * @param {float} quoteValue
   * @param {object} delta
   * @returns {object}
   */
  getDeltaDataByTotalData(quoteName, quoteValue, delta) {
    let quotesPoint = (this.firstData && this.firstData[quoteName]) ? this.firstData[quoteName]['Point'] : 1

    //calculate delta by OpenPrice
    this.constructor.forEachArr(this.totalData.month, (value) => {
      if (value.Symbol && value.Symbol === quoteName && value.Ticks) {
        let latestDataOfMonth = this.constructor.getLatestElementInArr(value.Ticks)

        if (latestDataOfMonth.OpenPrice) {
          delta.static = quoteValue - (latestDataOfMonth.OpenPrice * quotesPoint)
          delta.percent = delta.static / (latestDataOfMonth.OpenPrice * quotesPoint) * 100
          delta.cls = delta.static > 0 ? this.config.clsArray[0] : this.config.clsArray[1]
        }
      }
    })

    return delta
  }

  /**
   *
   * @param {string} quoteName
   * @param {float} quoteValue
   * @param {object} delta
   * @returns {object}
   */
  getDeltaDataByStore(quoteName, quoteValue, delta) {
    let clsIndex = null
    let quotesField = this.config.quotesField

    if (Object.keys(this.store).length) {
      //compare current quoteValue with previous value from store

      if (this.store[quoteName]) {
        if (quoteValue > this.store[quoteName][quotesField]) {
          clsIndex = 0
        } else if (quoteValue < this.store[quoteName][quotesField]) {
          clsIndex = 1
        }
      }

    } else {
      //get random index from array - for first render data
      clsIndex = Math.floor(Math.random() * this.config.clsArray.length)
    }

    if (clsIndex !== null) {
      delta.cls = this.config.clsArray[clsIndex]
    }

    return delta
  }

  getFilteredQuotesByData(data) {
    let filtered = {}

    if (data)
      this.constructor.forEachArr(this.config.quotesList, key => filtered[key] = data[key])

    return filtered
  }

  updateFirst(data) {
    if (!data || !data.Symbols) return

    this.firstData = {}
    this.constructor.forEachObj(data.Symbols, (value, key) => this.firstData[key] = value[0])
    this.changeDOM(this.firstData)
  }

  updateTotal(data) {
    if (!data) return

    this.totalData.month = data[0]
    this.totalData.day = data[1]
    this.changeDOM(this.firstData)
  }

  updateWatch(data) {
    if (!data) return
    this.changeDOM(data)
  }

  changeDOM(data) {
    if (!data) return

    let quotes = this.getFilteredQuotesByData(data)

    this.constructor.forEachObj(quotes, (value, key) => {
      if (value) {
        let delta = this.getDeltaData(value, key)
        delta.cls && this.updateHtml(value, delta)
      }
    })

    //save current data to store
    this.constructor.forEachObj(quotes, (value, key) => {
      if (value)
        this.store[key] = value
    })
  }

  toggleClassName(collections, removeArray, addArray) {
    this.constructor.forEachArr(collections, element => {
      removeArray.map(item => element.classList.remove(item))
      addArray.map(item => element.classList.add(item))
    })
  }

  createHtml(data, delta) {
    let html = ''
    let value = this.constructor.toFixed(data[this.config.quotesField], 5, 7)

    html += '<span>' + value + '</span>'

    return html
  }

  /**
   *
   * @param {object} data
   * @param {object} delta
   */
  updateHtml(data, delta) {
    if (!this.node || !data || !data.Symbol || !delta || !delta.cls) return
    let elements = this.node.querySelectorAll('.js-' + data.Symbol.toLowerCase())

    this.toggleClassName(elements, this.config.clsArray, [delta.cls])

    this.constructor.forEachArr(elements, element => {
      if (this.config.updateHtml) {
        element.innerHTML = this.createHtml(data, delta)
      } else {
        this.createHtml(data, delta)
      }
    })
  }

  show() {
    this.node && this.node.classList.add('is-loaded')
  }

  /**
   *
   * @param {array} elem
   * @param {string} event
   * @param {function} func //callback
   */
  static on (elem, event, func) {
    if (elem.addEventListener) {
      elem.addEventListener(event, func)
    } else {
      elem.attachEvent('on' + event, func)
    }
  }

  static forEachArr (arr, callback) {
    for (let i = 0; i < arr.length; i+=1) {
      callback(arr[i], i)
    }
  }

  static forEachObj (obj, callback) {
    for (let key in obj) {
      if (obj.hasOwnProperty(key) && callback) callback(obj[key], key)
    }
  }

  static getLatestElementInArr(arr) {
    return arr[arr.length-1]
  }

  /**
   *
   * @param {*} value
   * @param {number} digits
   * @param {number} maxDigits - optional
   * @returns {float}
   */
  static toFixed(value, digits, maxDigits) {
    typeof value !== 'number' && (value = 0)
    let result = value.toFixed(digits)

    //0.00 -> 0.001
    while(Math.abs(result) === 0) {
      digits += 1
      result = value.toFixed(digits)
    }

    if (maxDigits) {
      let temp = result.toString().split('.')
      if (temp[1] && temp[1].length > maxDigits) result.toFixed(maxDigits)
    }

    return parseFloat(result)
  }
}