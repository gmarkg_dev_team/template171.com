/**
 * dependencies: axios, es6-promise, socketcluster-client
 *
 * example one item from firstData
 * {
 *  Symbol: 'quoteName'
    Ctm: number
    Bid: decimal
    Ask: decimal
    Currency: 'quoteName'
    ContractSize: number
    TickValue: number
    TickSize: number
    ProfitMode: string
    Point: decimal
    MarginMode: string
    MarginHedged: number
    MarginInitial: number
    MarginDivider: number
    MarginMaintenance: number
 * }
 *
 * example one item from totalData
 * {
    Symbol: 'quoteName'
    Ticks: [
      {
        RateTime: number
        OpenPrice: number
        HighShiftOpen: number
        LowShiftOpen: number
        CloseShiftOpen: number
      },
    ],
  },
 *
 */

import axios from 'axios'
import socketcluster from 'socketcluster-client'

export default class QuotesWatcher {
  constructor() {
    this.socket = socketcluster.create({
      hostname: `${process.env.WIDGET_TRADING_DOMAIN}`,
      port: `${process.env.WIDGET_TRADING_PORT}`,
      secure: true,
      rejectUnauthorized: false,
    })
    this.config = {
      'apiUrl':         '/api/trading',
      'headers':        {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${process.env.WIDGET_TRADING_AUTH_KEY}`
      },
      'fetchTotalData': false, //fetch data for all quotes of last month and last day
      'watching'      : false, //watching websocket, tick every second
    }
    this.chanel = this.socket.subscribe('symbolTicker_prod') //subscribe to websocket
  }

  init() {
    this
        .fetchData( `${this.config.apiUrl}/symbols`, null)
        .then(response => {
              this.callback('getFirstData', response)

              return this.config.fetchTotalData ? this.getTotalData() : null
            },
            error => {
              console.error('getFirstData: ', error)
            })

        .then(response => {
              if (response)
                this.callback('getTotalData', response)

              if (this.config.watching) //start watching? tick every second
                this.chanel.watch(obj => this.callback('watch', JSON.parse(obj)) )
            },
            error => {
              console.error('getTotalDataError: ', error)
            })
  }

  /**
   *
   * @param {string} url
   * @param {Object} queryData
   * @returns {Promise<any> | * | Promise}
   */
  fetchData(url, queryData) {
    return new Promise((resolve, reject) => {
      axios({
        method: 'POST',
        url: url,
        data: !!queryData ? queryData : {},
        headers: this.config.headers,
      })
          .then(response => resolve(response.data))
          .catch(error => reject(error))
          .finally(() => {
            //always executed
          })
    })
  }

  /**
   * &allSymbols=true - get data for all quotes
   * &symbols=BTCUSD&symbols=EURUSD - get data for some quotes
   * @returns {Promise<any> | * | Promise}
   */
  getTotalData() {
    let urlParams = '&allSymbols=true'
    let p1 = this.fetchData(`${this.config.apiUrl}/report`, {'query': `period=month${urlParams}`}) //fetch data of last month
    let p2 = this.fetchData(`${this.config.apiUrl}/report`, {'query': `period=day${urlParams}`})   //fetch data of last day

    return Promise.all([p1, p2])
        .then(values => values, error => error)
  }

  /**
   *
   * @param {string} type - example: getFirstData, getTotalData, watch
   * @param {object || array} response
   */
  callback(type, response) {
    console.log('type - ', type, ':', 'response - ', response) //you need to rewrite this method in extended class
  }

  static forEachArr (arr, callback) {
    for (let i = 0; i < arr.length; i+=1) {
      callback(arr[i], i)
    }
  }

  static forEachObj (obj, callback) {
    for (let key in obj) {
      if (obj.hasOwnProperty(key) && callback) callback(obj[key], key)
    }
  }
}
