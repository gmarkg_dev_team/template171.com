<?php

return [
  'button' => [
    'home' => 'Inicio',
    'home_back' => 'Volver a la página de inicio',
    'send_message' => 'Enviar mensaje'
  ],
  'breadcrumbs' => [
    'home' => 'Inicio',
  ]
];