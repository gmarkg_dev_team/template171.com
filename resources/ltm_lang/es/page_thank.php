<?php

return [
  'title'    => [
    'default'  => '¡Gracias por enviarnos un mensaje!',
    'callback' => 'Su solicitud es aceptada'
  ],
  'subtitle' => 'Pronto uno de nuestros gerentes se comunicará con usted',
];