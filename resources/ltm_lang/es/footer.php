<?php

return [
  'risk' => 'INFORMACIÓN Y ALERTA DE ALTO RIESGO:',
  'risk_text' => 'Operar con divisas (FX) y metales preciosos conlleva un alto nivel de riesgo que puede no ser adecuado para todos los inversores. El apalancamiento genera un riesgo adicional y una exposición a pérdidas. Antes que decidas operar, considera cuidadosamente tus objetivos de inversión, nivel de experiencia y tolerancia al riesgo.',
  'legal' => 'EXCLUSIÓN DE GARANTÍAS Y RESPONSABILIDAD:',
  'legal_text' => 'Operar con divisas implica riesgos significativos y no es apropiado para todos los inversores. Debes tener en cuenta la posibilidad de que se produzcan pérdidas sustanciales. Por lo tanto, es importante que comprendas las posibles consecuencias de tu inversión. Los operadores deben sopesar su potencial de ingresos en relación con los riesgos implicados y actuar en consecuencia.',
  'copy' => "© " . date('Y') . " " . setting('site.title') . " Todos los derechos reservados",
];
