<?php

return [
  'risk' => 'INFORMATIONEN UND WARNUNG VOR HOHEM RISIKO:',
  'risk_text' => 'Der Handel mit Devisen (FX) und Edelmetallen trägt ein hohes Risiko, das nicht für alle Anleger geeignet ist. Leverage schafft zusätzliche Risiken und Verluste. Bevor Sie sich entscheiden zu handeln, sollten Sie Ihre Anlageziele, Ihr Erfahrungsniveau und Ihre Risikotoleranz sorgfältig prüfen.',
  'legal' => 'HAFTUNGSAUSSCHLUSS:',
  'legal_text' => 'Der Devisenhandel ist mit erheblichen Risiken verbunden und eignet sich nicht für alle Anleger. Die Möglichkeit erheblicher Verluste sollte berücksichtigt werden. Es ist daher wichtig, die möglichen Folgen von Investitionen zu verstehen. Händler sollten ihr Verdienstpotenzial gegen die damit verbundenen Risiken abwägen und entsprechend handeln.',
  'copy' => "© " . date('Y') . " " . setting('site.title') . " Alle Rechte vorbehalten",
];
