<?php

return [
  'ajax' => 'Etwas ist schief gelaufen! Bitte versuchen Sie es später erneut.',
  'email' => 'Diese E-Mail existiert bereits',
  'phone' => 'Diese Telefonnummer existiert bereits',
  'field' => 'Feld ist falsch ausgefüllt'
];