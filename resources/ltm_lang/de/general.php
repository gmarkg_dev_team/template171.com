<?php

return [
  'button' => [
    'home' => 'Startseite',
    'home_back' => 'Zur Startseite',
    'send_message' => 'Nachricht senden'
  ],
  'breadcrumbs' => [
    'home' => 'Startseite',
  ]
];