<?php

return [
  'title'    => [
    'default'  => 'Danke für deine Registrierung!',
    'callback' => 'Ihre Anfrage wird angenommen.'
  ],
  'subtitle' => 'In Kürze wird sich einer unserer Manager mit Ihnen in Verbindung setzen',
];