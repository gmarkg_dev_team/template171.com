<?php

return [
  'titles' => [
    'main' => 'Formulaire d\'inscription',
    'demo' => 'Ouvrir un compte démo',
    'live' => 'Inscrivez-vous pour commencer à trader',
    'contact' => 'Posez n\'importe quelle question'
  ],
  'labels' => [
    'name' => 'Prénom',
    'last_name' => 'Nom de famille',
    'email' => 'Email',
    'country' => 'Pays',
    'phone' => 'Numéro de télèphone',
    'promo' => 'Code Promotion',
    'lang' => 'Langue',
    'subject' => 'Sujet',
    'message' => 'Message',
    'website' => 'Site Web',
    'company' => 'Nom de l\'entreprise',
    'street' => 'Rue',
    'city' => 'Ville',
    'zip' => 'Code postal/Zip',
    'type' => 'Type de messagerie instantanée',
    'im' => 'Compte de messagerie instantanée',
    'callback' => 'Numéro de télèphone',
  ],
  'placeholders' => [
    'name' => 'Entrez le prénom',
    'last_name' => 'Entrez le nom',
    'email' => 'Entrez l\'email',
    'country' => 'Choisissez un pays',
    'phone' => 'Entrez le numéro de téléphone',
    'promo' => 'Saisir le code promotionnel',
    'lang' => 'Choisissez la langue',
    'subject' => 'Sujet',
    'message' => 'Entrez votre question',
    'website' => 'Entrez sur le site Web',
    'company' => 'Entrez le nom de l\'entreprise',
    'street' => 'Rue',
    'city' => 'Ville',
    'zip' => 'Code postal/Zip',
    'type' => 'Choisissez votre type de messagerie instantanée',
    'im' => 'Compte de messagerie instantanée',
    'callback' => 'Entrez votre numéro',
  ],
  'terms' => [
    'text' => 'J\'accepte les',
    'link' => 'Termes et conditions',
    'edge' => 'J\'ai plus de 18 ans'
  ],
  'lang' => [
    'en' => 'Anglais',
    'fr' => 'Français',
    'de' => 'Allemand',
    'it' => 'Italien',
    'ru' => 'Russe',
    'ar' => 'Arabe',
    'pl' => 'Polonais',
    'es' => 'Espanol',
  ],
  'ajax' => [
    'success' => 'Votre demande est acceptée. <br> Merci',
    'error' => 'Un problème est survenu!',
  ],
];