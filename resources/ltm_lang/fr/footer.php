<?php

return [
  'risk' => "INFORMATIONS ET AVERTISSEMENT À HAUT RISQUE:",
  'risk_text' => "La négociation avec des devises (FX), des contrats pour différences (CFD) et des métaux précieux comporte un niveau de risque élevé qui peut ne pas convenir à tous les investisseurs. L'effet de levier crée une exposition supplémentaire aux risques et aux pertes. Avant de décider d\'échanger des devises ou de contracter des différences, considérez attentivement vos objectifs de placement, votre niveau d\'expérience et votre tolérance au risque.",
  'legal' => "AVERTISSEMENT LÉGAL:",
  'legal_text' => "Le trading sur le Forex comporte des risques importants et ne convient pas à tous les investisseurs. La possibilité de subir des pertes substantielles doit être prise en compte. Il est donc important de comprendre les conséquences possibles de l\'investissement. Les traders doivent peser leur potentiel de gain par rapport aux risques encourus et agir en conséquence.",
  'copy' => "© " . date('Y') . " " . setting('site.title') . "Trade Tous les droits sont réservés",
];
