<?php

return [
  'ajax' => 'Un problème est survenu! Veuillez réessayer plus tard.',
  'email' => 'Cet e-mail existe déjà',
  'phone' => 'Ce numéro de téléphone existe déjà',
  'field' => 'Champ mal rempli'
];