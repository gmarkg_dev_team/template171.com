<?php

return [
  'title'    => [
    'default'  => 'Merci de nous envoyer un message!',
    'callback' => 'Votre demande est acceptée.'
  ],
  'subtitle' => 'Bientôt, nos représentants vous contacteront.',
];