<?php

return [
  'button' => [
    'home' => 'Page d\'accueil',
    'home_back' => 'Retour à l\'accueil',
    'send_message' => 'Envoyer un message'
  ],
  'breadcrumbs' => [
    'home' => 'Page d\'accueil',
  ]
];