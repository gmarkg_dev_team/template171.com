<?php

return [
  'button' => [
    'login' => 'Log In',
    'registration' => 'Sign Up',
  ],
];
