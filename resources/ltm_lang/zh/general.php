<?php

return [
  'button' => [
    'home' => 'Home',
    'home_back' => 'Back home',
    'send_message' => 'Send message'
  ],
  'breadcrumbs' => [
    'home' => 'Home',
  ]
];