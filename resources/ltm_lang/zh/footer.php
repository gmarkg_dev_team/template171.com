<?php

return [
  'risk' => 'INFORMATION AND HIGH RISK WARNING:',
  'risk_text' => 'Trading with foreign exchange (FX) and precious metals carries a high level of risk that may not be suitable for all investors. Leverage creates additional risk and loss exposure. Before you decide to trade, carefully consider your investment objectives, experience level, and risk tolerance.',
  'legal' => 'LEGAL DISCLAIMER:',
  'legal_text' => 'Forex trading entails significant risks and is not appropriate for all investors. The possibility of incurring substantial losses should be taken into account. It is therefore important to understand the possible consequences of investing. Traders should weigh their earning potential against the risks involved and act accordingly.',
  'copy' => "© " . date('Y') . " " . setting('site.title') . " All rights reserved",
  'docs' => [
    'risk_warnings' => 'Risk warnings',
    'terms_and_conditions' => 'Terms and conditions',
    'privacy_policy' => 'Privacy policy',
    'anti_money_laundering' => 'Anti-money laundering'
  ],
];