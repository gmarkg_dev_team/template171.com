<?php

return [
  'ajax' => 'Something went wrong! Please try again later.',
  'email' => 'This email already exists',
  'phone' => 'This phone number already exists',
  'field' => 'Incorrectly filled field'
];