<?php

return [
  'button' => [
    'home' => 'Home',
    'home_back' => 'Back Home',
    'send_message' => 'Send Message'
  ],
  'breadcrumbs' => [
    'home' => 'Home',
  ]
];