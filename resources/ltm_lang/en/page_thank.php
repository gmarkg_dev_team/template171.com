<?php

return [
  'title'    => [
    'default'  => 'Thank you for messaging us!',
    'callback' => 'Your request is accepted.'
  ],
  'subtitle' => 'Soon one of our managers will contact you',
];