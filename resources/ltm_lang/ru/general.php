<?php

return [
  'button' => [
    'home' => 'Главная',
    'home_back' => 'Назад на Главную',
    'send_message' => 'Отправить сообщение'
  ],
  'breadcrumbs' => [
    'home' => 'Главная',
  ]
];