<?php

return [
  'risk' => 'ИНФОРМАЦИЯ И УВЕДОМЛЕНИЕ О ВЫСОКИХ РИСКАХ:',
  'risk_text' => 'Торговля иностранной валютой (FX) и драгоценными металлами сопряжена с высоким уровнем риска, который может не подходить для всех инвесторов. Кредитное плечо создаёт дополнительный риск и возможности потерь. Прежде чем принять решение о торговле, тщательно продумайте свои инвестиционные цели, уровень опыта и допустимость рисков.',
  'legal' => 'ОТКАЗ ОТ ОТВЕТСТВЕННОСТИ:',
  'legal_text' => 'Торговля валютой сопряжена со значительными рисками и приемлема не для всех инвесторов. Возможность значительных потерь также должна быть учтена. Важно понимать все возможные последствия, связанные с инвестированием. Трейдеры должны взвешивать их возможность заработка относительно существующих рисков и действовать аккуратно.',
  'copy' => "© " . date('Y') . " " . setting('site.title') . " Все права защищены",
];
