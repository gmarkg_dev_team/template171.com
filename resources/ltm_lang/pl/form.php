<?php

return [
  'titles' => [
    'main' => 'Formularz rejestracyjny',
    'demo' => 'Otwórz konto demo',
    'live' => 'Zarejestruj się, aby rozpocząć handel',
    'contact' => 'Zadaj dowolne pytanie'
  ],
  'labels' => [
    'name' => 'Imię',
    'last_name' => 'Nazwisko',
    'email' => 'Email',
    'country' => 'Kraj',
    'phone' => 'Numer telefonu',
    'promo' => 'Kod promocyjny',
    'lang' => 'Język',
    'subject' => 'Motyw',
    'message' => 'Wiadomość',
    'website' => 'Strona internetowa',
    'company' => 'Nazwa firmy',
    'street' => 'Ulica',
    'city' => 'Miasto',
    'zip' => 'Kod pocztowy',
    'type' => 'Typ IM konto',
    'im' => 'Konto IM',
    'callback' => 'Numer telefonu',
  ],
  'placeholders' => [
    'name' => 'Wprowadź imię',
    'last_name' => 'Wpisz nazwisko',
    'email' => 'Wprowadź email',
    'country' => 'Wybierz kraj',
    'phone' => 'Wpisz numer telefonu',
    'promo' => 'Wpisz kod promocyjny',
    'lang' => 'Wybierz język',
    'subject' => 'Motyw',
    'message' => 'Wpisz swoje pytanie',
    'website' => 'Wprowadź internetowąт',
    'company' => 'Wpisz nazwę firmy',
    'street' => 'Ulica',
    'city' => 'Miasto',
    'zip' => 'Kod pocztowy',
    'type' => 'Wybierz typ IM',
    'im' => 'Konto IM',
    'callback' => 'Wprowadź swój numer',
  ],
  'terms' => [
    'text' => 'Zgadzam się z',
    'link' => 'zasady i warunki',
    'edge' => 'Mam ponad 18 lat'
  ],
  'lang' => [
    'en' => 'Angielski',
    'fr' => 'Francuski',
    'de' => 'Niemiecki',
    'it' => 'Włoski',
    'ru' => 'Rosyjski',
    'ar' => 'Arabski',
    'pl' => 'Polski',
    'es' => 'Hiszpański',
  ],
  'ajax' => [
    'success' => 'Twoja prośba została zaakceptowana. <br> Dziękuję',
    'error' => 'Coś poszło nie tak!',
  ],
];