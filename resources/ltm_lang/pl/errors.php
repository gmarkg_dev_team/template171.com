<?php

return [
  'ajax' => 'Coś poszło nie tak! Spróbuj ponownie później.',
  'email' => 'Ten email już istnieje',
  'phone' => 'Ten numer telefonu już istnieje',
  'field' => 'Nieprawidłowo wypełnione pole'
];