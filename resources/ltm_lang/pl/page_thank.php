<?php

return [
  'title'    => [
    'default'  => 'Dziękuję za wiadomość!',
    'callback' => 'Twoja prośba została zaakceptowana.'
  ],
  'subtitle' => 'Wkrótce skontaktuje się z Tobą jeden z naszych menedżerów',
];