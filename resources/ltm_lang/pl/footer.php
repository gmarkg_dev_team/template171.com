<?php

return [
  'risk' => 'INFORMACJE I OSTRZEŻENIE O WYSOKIM RYZYKU:',
  'risk_text' => 'Handel walutami (FX) i metalami szlachetnymi niosą ze sobą wysoki poziom ryzyka, który może nie być odpowiedni dla wszystkich inwestorów. Dźwignia stwarza dodatkowe ryzyko, także ryzyko strat. Zanim zdecydujesz się na handel, dokładnie rozważ swoje cele inwestycyjne, poziom doświadczenia i tolerancję ryzyka.',
  'legal' => 'ZASTRZEŻENIA PRAWNE:',
  'legal_text' => 'Handel na rynku Forex wiąże się ze znacznym ryzyko i nie jest odpowiedni dla wszystkich inwestorów. Należy wziąć pod uwagę możliwość znacznych strat. Dlatego ważne jest, aby zrozumieć ewentualne konsekwencje inwestowania. Handlowcy muszą dopasować swój potencjalny dochód do odpowiedniego ryzyka i odpowiednio postępować.',
  'copy' => "© " . date('Y') . " " . setting('site.title') . " Wszelkie prawa zastrzeżone",
];
