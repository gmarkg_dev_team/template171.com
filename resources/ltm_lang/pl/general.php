<?php

return [
  'button' => [
    'home' => 'Strona główna',
    'home_back' => 'Powrót do strony głównej',
    'send_message' => 'Wyślij wiadomość'
  ],
  'breadcrumbs' => [
    'home' => 'Strona główna',
  ]
];