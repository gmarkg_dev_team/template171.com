<?php

return [
  'visitors' => [
    'title' => 'Visitors',
    'text' => 'You have <span>0</span> visitors on your site. Click on button below to view all visitors.',
    'btn text' => 'View all visits'
  ],
  'slider-quotes' => [
    'text' => 'INVEST RESPONSIBLY:<br>Trading CFDs involves significant risks.',
  ],
  'table-quotes' => [
    'price-title' => 'Buy price',
    'percent-title' => 'Change over the period',
  ],
  'categories-quotes' => [
    'navs' => [
      'forex' => 'Forex',
      'shares' => 'Shares',
      'commodities' => 'Commodities',
      'indices' => 'Indices',
    ],
    'cols' => ['Market', 'Bid', 'Ask'],
  ],
];