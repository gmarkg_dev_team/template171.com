<?php

return [
  'title'    => [
    'default'  => 'Grazie per averci inviato un messaggio!',
    'callback' => 'La tua richiesta è stata accettata.'
  ],
  'subtitle' => 'Presto un nostro responsabile ti contatterà',
];