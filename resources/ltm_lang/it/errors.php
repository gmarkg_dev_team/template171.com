<?php

return [
  'ajax' => 'Qualcosa è andato storto! Si prega di riprovare più tardi.',
  'email' => 'Questa e-mail esiste già',
  'phone' => 'Questo numero di telefono esiste già',
  'field' => 'Campo compilato in modo errato'
];