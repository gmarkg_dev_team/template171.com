<?php

return [
  'title' => 'Access Denied!',
  'text' => 'Access Denied! This website isn\'t available in your country.',
];
