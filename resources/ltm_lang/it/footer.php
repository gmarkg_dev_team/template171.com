<?php

return [
  'risk' => 'INFORMAZIONI ED AVVISO DI ALTO RISCHIO:',
  'risk_text' => 'Il trading con i cambi (FX) i metalli preziosi comportano un alto livello di rischio che potrebbe non essere adatto a tutti gli investitori. La leva finanziaria crea un\'ulteriore esposizione al rischio ed alle perdite. Prima di decidere di scambiare studiate attentamente gli obiettivi di investimento, il livello di esperienza e la tolleranza al rischio.',
  'legal' => 'ESCLUSIONE DI RESPONSABILITÀ LEGALE:',
  'legal_text' => 'Il trading sul Forex comporta rischi significativi e non è appropriato per tutti gli investitori. È opportuno tener conto della possibilità di incorrere in perdite sostanziali. Quindi, è importante comprendere le possibili conseguenze degli investimenti. I trader dovrebbero valutare il loro potenziale di guadagno rispetto ai rischi in questione ed agire di conseguenza.',
  'copy' => "© " . date('Y') . " " . setting('site.title') . " Tutti i diritti riservati",
];
