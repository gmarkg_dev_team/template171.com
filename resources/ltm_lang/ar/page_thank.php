<?php

return [
  'title'    => [
    'default'  => 'شكرا لك على مراسلتنا!',
    'callback' => 'تم قبول طلبك.'
  ],
  'subtitle' => 'قريباً سيتصل بك أحد مديرينا',
];