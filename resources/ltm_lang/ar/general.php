<?php

return [
  'button' => [
    'home' => 'الصفحة الرئيسية',
    'home_back' => 'العودة إلى الصفحة الرئيسية',
    'send_message' => 'أرسل رسالة'
  ],
  'breadcrumbs' => [
    'home' => 'الصفحة الرئيسية',
  ]
];