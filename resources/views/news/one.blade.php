@component('layouts.index')
  @slot('title',!empty($news->getTranslatedAttribute('seo_title')) ? $news->getTranslatedAttribute('seo_title') : $news->getTranslatedAttribute('title').' - '.setting('site.title'))
  @slot('description', $news->getTranslatedAttribute('seo_description'))
  @slot('pageCss','page')
  @slot('pageJs','page')

  <main class="site-content pt page-news__one" data-room="{{$socketRoom}}">
    @include("components.bread",['title' => $news->getTranslatedAttribute('title'),'titlePar' => __('page_news.title'),'route' => route('news'),'parent' => false])
    <article class="box gui content-padding">
      <h1 id="h1">{{$news->getTranslatedAttribute('title')}}</h1>
      <div class="gui__date">
        @php($date = $news->getOrderColumn())
        @php($time = \Carbon\Carbon::parse($news->$date))
        <time>{{$time->format('d.m.Y')}}</time>
      </div>
      <figure class="gui__img">
        <img src="{{$news->imageUrl()}}" alt="{{$news->getTranslatedAttribute('image_alt')}}">
      </figure>

      {!! $news->getTranslatedAttribute('content') !!}
      <div style="clear: both"></div>
    </article>
  </main>

@endcomponent