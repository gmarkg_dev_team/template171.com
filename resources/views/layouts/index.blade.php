<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
  @include('engine::components.disallow-indexing')

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>{{$title ?? setting('site.title')}}</title>

  {{$meta ?? ''}}

  @if(isset($localizedUrls))
    <link rel="canonical" href="{{$localizedUrls[$language]}}"/>

    @foreach($localizedUrls as $lang=>$key)
      <link rel="alternate" hreflang="{{$lang}}" href="{{$key}}"/>
    @endforeach
  @endif

  <meta name="description" content="{{$description ?? setting('site.description')}}"/>
  <meta property="og:site_name" content="{{$title ?? setting('site.title')}}"/>
  <meta property="og:title" content="{{$title ?? setting('site.title')}}"/>
  <meta property="og:type" content="article"/>

  @if(isset($localizedUrls))
    <meta property="og:url" content="{{$localizedUrls[$language]}}"/>
  @endif

  @if(isset($language))
    <meta property="og:locale" content="{{LaravelLocalization::getSupportedLocales()[$language]['regional']}}"/>
  @endif

  <meta property="og:description" content="{{$description ?? setting('site.description')}}"/>
  <meta name="twitter:card" content="summary"/>
  <meta name="twitter:title" content="{{$title ?? setting('site.title')}}"/>
  <meta name="twitter:description" content="{{$description ?? setting('site.description')}}"/>

  @php($faviconPath = !empty(config('engine.favicon_path')) ? config('engine.favicon_path') : '/favicon/')

  <link rel="apple-touch-icon" sizes="180x180" href="{{asset($faviconPath.'apple-touch-icon.png')}}">
  <link rel="icon" type="image/png" sizes="32x32" href="{{asset($faviconPath.'favicon-32x32.png')}}">
  <link rel="icon" type="image/png" sizes="16x16" href="{{asset($faviconPath.'favicon-16x16.png')}}">
  <link rel="manifest" href="{{asset($faviconPath.'site.webmanifest')}}">
  <link rel="mask-icon" href="{{asset($faviconPath.'safari-pinned-tab.svg')}}" color="#5bbad5">
  <link rel="shortcut icon" href="{{asset($faviconPath.'favicon.ico')}}">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="msapplication-config" content="{{asset($faviconPath.'browserconfig.xml')}}">
  <meta name="theme-color" content="#ffffff">

  <link rel="stylesheet" href="{{ mix('css/main.css') }}">
</head>
<body>

<div id="wrapper">
  @if (!isset($acc) && !isset($header))
    {{-- @include("components.header") --}}
  @endif

  {{-- dinamic part. define in each folder of page --}}
  {{ $slot }}
  {{-- end dinamic part --}}

  @if (!isset($acc) && !isset($footer))
    {{-- @include ("components.footer") --}}
  @endif

</div>

<script type="text/javascript" src="{{ mix('js/app.js') }}"></script>
</body>
</html>
