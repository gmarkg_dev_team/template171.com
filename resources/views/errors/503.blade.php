@component('layouts.index')
  @slot('title','503 - '.setting('site.title'))
  @slot('pageCss','503')
  @slot('pageJs','page')
  @slot('acc', '')

  <main class="site-content page-error" data-room="error">
    <div class="error box">
      <article class="error__text">
        <h1 class="error__503">503</h1>
        <p class="error__oops">{!! __('page_503.text') !!}</p>
      </article>
    </div>
  </main>

@endcomponent