@component('layouts.index')
  @slot('title','404 - '.setting('site.title'))
  @slot('pageCss','404')
  @slot('pageJs','page')

  <main class="site-content pt page-error" data-room="error">
    @include("components.bread",['title' => 404,'parent' => true])
    <div class="error box">
      <article class="error__text">
        <h1 class="error__404">404</h1>
        <p class="error__oops">{!! __('page_404.text') !!}</p>
        <a class="error__btn" href="{{route('home')}}">{{__('general.button.home_back')}}</a>
      </article>
    </div>
  </main>

@endcomponent