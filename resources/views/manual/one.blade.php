@component('layouts.index')
  @slot('title',!empty($manual->getTranslatedAttribute('seo_title')) ? $manual->getTranslatedAttribute('seo_title') : $manual->getTranslatedAttribute('question').' - '.setting('site.title'))
  @slot('description', $manual->getTranslatedAttribute('seo_description'))
  @slot('pageCss','page')
  @slot('pageJs','page')

  <main class="site-content pt page-faq__one" data-room="{{$socketRoom}}">
    @include("components.bread",['title' => $manual->getTranslatedAttribute('question'),'titlePar' => __('page_manual.title'),'route' => route('manuals'),'parent' => false])
    <article class="box gui content-padding">
      <h1 id="h1">{{$manual->getTranslatedAttribute('question') }}</h1>
      {!! $manual->getTranslatedAttribute('answer') !!}
      <div style="clear: both"></div>
    </article>
  </main>

@endcomponent