@component('layouts.index')
  @php($title = __('page_documents.title'))

  @slot('title',$title.' - '.setting('site.title'))
	@slot('pageCss','faq')
	@slot('pageJs','page')

	<main class="site-content pt page-faq" data-room="{{$socketRoom}}">
		@include("components.bread",['title' => $title,'parent' => true])
		<article class="box faq content-padding">
			<h1 id="h1" class="title">{{$title}}</h1>

			@if(!empty($docs))
				<ol>
					@foreach($docs as $key => $item)
						<li>
							<a href="{{ route('documents.one', ['page' => $item->getTranslatedAttribute('slug')]) }}">{{ $item->getTranslatedAttribute('title') }}</a>
						</li>
					@endforeach
				</ol>
			@endif
		</article>
	</main>

@endcomponent