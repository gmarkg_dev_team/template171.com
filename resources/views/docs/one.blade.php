@component('layouts.index')
	@slot('title',!empty($doc->getTranslatedAttribute('seo_title')) ? $doc->getTranslatedAttribute('seo_title') : $doc->getTranslatedAttribute('title').' - '.setting('site.title'))
	@slot('description', $doc->getTranslatedAttribute('seo_description'))
	@slot('pageCss','docs')
	@slot('pageJs','docs')

	<main class="site-content pt page-docs__one" data-room="{{$socketRoom}}">
		@include("components.bread",['title' => $doc->getTranslatedAttribute('title'),'titlePar' => __('engine.titles.docs'),'route' => route('documents'),'parent' => false])

		<article class="box docs__gui content-padding">
			<h1 id="h1">{{ $doc->getTranslatedAttribute('enable_translation') ? $doc->getTranslatedAttribute('title') : $doc->getTranslatedAttribute('title', 'en') }}</h1>
			{!! $doc->getTranslatedAttribute('enable_translation') ? $doc->getTranslatedAttribute('content') : $doc->getTranslatedAttribute('content', 'en') !!}
			<div style="clear: both"></div>
		</article>
	</main>

@endcomponent
