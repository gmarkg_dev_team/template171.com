@component('layouts.index')
  @php($title = __('page_thank.title.callback') )
  @php($subtitle = empty(request()->get('subtitle')) ? __('page_thank.subtitle') : request()->get('subtitle'))
  @slot('title', strip_tags($title).' - '.setting('site.title'))
  @slot('pageCss','thank')
  @slot('pageJs','page')

  <main class="site-content pt page-thank" data-room="thank">
    @include("components.bread", ['title' => strip_tags($title), 'parent' => true])
    <div class="thank box">
      <article class="thank__text">
        <h1 class="thank__title title">{!! $title !!}</h1>
        <p class="thank__subtitle">{!! $subtitle !!}</p>
        <a class="thank__btn" href="{{ route('home') }}">{{__('general.button.home_back')}}</a>
      </article>
    </div>
  </main>

@endcomponent
