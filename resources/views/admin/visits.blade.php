@extends('voyager::master')

@section('page_header')
    <h1 class="page-title">
        Visits
    </h1>
@stop

@section('content')
    <div class="page-content browse container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <div id="dataTable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <canvas id="visits" height="80"></canvas>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <table id="dataTable" class="table table-hover dataTable no-footer visits"
                                               role="grid" aria-describedby="dataTable_info">
                                            <thead>
                                            <tr role="row">
                                                <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1"
                                                    colspan="1" aria-label="Room">
                                                    Room
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1"
                                                    colspan="1" aria-label="Visitors">
                                                    Visitors
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr class="user__rooms" role="row" data-room="all">
                                                <td class="user__room">
                                                    All
                                                </td>
                                                <td class="user__num">
                                                    0
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection