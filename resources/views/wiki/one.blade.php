@component('layouts.index')
  @slot('title',!empty($wiki->getTranslatedAttribute('seo_title')) ? $wiki->getTranslatedAttribute('seo_title') : $wiki->getTranslatedAttribute('term').' - '.setting('site.title'))
  @slot('description', $wiki->getTranslatedAttribute('seo_description'))
  @slot('pageCss','page')
  @slot('pageJs','page')

  <main class="site-content pt page-wiki__one" data-room="{{$socketRoom}}">
    @include("components.bread",['title' => $wiki->getTranslatedAttribute('term'),'titlePar' => __('page_wiki.title'),'route' => route('wiki'),'parent' => false])
    <article class="box gui content-padding">
      <h1 id="h1">{{ $wiki->getTranslatedAttribute('term') }}</h1>
      {!! $wiki->getTranslatedAttribute('content') !!}
      <div style="clear: both"></div>
    </article>
  </main>

@endcomponent