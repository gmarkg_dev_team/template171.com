@component('layouts.index')
  @php($title = __('page_wiki.title'))

  @slot('title',$title.' - '.setting('site.title'))
  @slot('pageCss','wiki')
  @slot('pageJs','wiki')

  <main class="site-content pt page-wiki" data-room="{{$socketRoom}}">
    @include("components.bread",['title' => $title,'parent' => true])
    <article class="box wiki content-padding">
      <h1 id="h1" class="wiki__title title">{{$title}}</h1>
      <nav class="wiki__alf">
        <ul class="wiki__alf--list">
          @if (!empty($wiki))
            @foreach ($wiki as $letter => $key)
              @if(count($key)>0)
                <li><a class="wiki-to" href="#letter_{{$letter}}">{{$letter}}</a></li>
              @else
                <li class="disabled"><span>{{$letter}}</span></li>
              @endif
            @endforeach
          @endif
        </ul>
      </nav>
      <div class="wiki__block">
        @if (!empty($wiki))
          @foreach ($wiki as $letter => $key)
            @if(count($key)>0)
              <section id="letter_{{$letter}}" class="wiki__block--list">
                <h4 class="wiki__block--letter">{{$letter}}</h4>
                @foreach($key as $word)
                  <div class="wiki__block--word">
										<a href="{{route('wiki.one',['page'=>$word->getTranslatedAttribute('slug')])}}"
											 class="wiki__block--h5">
											<span class="wiki__block--title">{{$word->getTranslatedAttribute('term')}} </span>
											<span class="wiki__block--trans">{{$word->getTranslatedAttribute('term','en')}}</span>
										</a>
                    <div class="wiki__block--desc">{!! $word->getTranslatedAttribute('content') !!}</div>
                  </div>
                @endforeach
              </section>
            @endif
          @endforeach
        @endif
      </div>
    </article>
  </main>

@endcomponent