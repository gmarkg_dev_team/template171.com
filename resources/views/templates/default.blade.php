@component('layouts.index')
  @slot('title',!empty($page->getTranslatedAttribute('seo_title')) ? $page->getTranslatedAttribute('seo_title') : $page->getTranslatedAttribute('title').' - '.setting('site.title'))
  @slot('description', $page->getTranslatedAttribute('seo_description'))
  @slot('pageCss','page')
  @slot('pageJs','page')

  <main class="site-content pt page-page" data-room="{{$socketRoom}}" data-roomAll="page">
    @include("components.bread",['title' => $page->getTranslatedAttribute('title'),'parent' => true])
    <article class="box gui content-padding">
      <h1
        id="h1">{{ $page->getTranslatedAttribute('title') }}</h1>
      {!! $page->getTranslatedAttribute('content') !!}
      <div style="clear: both"></div>
    </article>
  </main>

@endcomponent