@if(!empty($items))
  <nav class="header__nav">
    <ul class="header__menu--list">
      @foreach($items as $menu_item)
        @php
          if (!empty($menu_item->url)){
            if ($language === 'en'){
              $url_all = $menu_item->url;
            } else {
              $url_all = '/'.$language.$menu_item->url;
            }
          } else {
            $url_all =  $menu_item->link();
          }
        @endphp
        <li class="header__menu--child">
          @if(url()->current() != env('APP_URL').$url_all && !empty($url_all))
            <a href="{{$url_all}}" class="header__menu--link">{{ $menu_item->getTranslatedAttribute('title') }}
              @if($menu_item->children->count())
                <span class="header__menu--arrow js-arrow"></span>
              @endif
            </a>
          @else
            <span class="header__menu--link @if(url()->current() === env('APP_URL').$url_all) active @endif">{{ $menu_item->getTranslatedAttribute('title') }}
              @if($menu_item->children->count())
                <span class="header__menu--arrow js-arrow"></span>
              @endif
        </span>
          @endif
          @if($menu_item->children->count())
            <ul class="header__submenu--list">
              @foreach($menu_item->children as $menu_children)
                @php
                  if (!empty($menu_children->url)){
                    if ($language === 'en'){
                      $url_all_child = $menu_children->url;
                    } else {
                      $url_all_child = '/'.$language.$menu_children->url;
                    }
                  } else {
                    $url_all_child =  $menu_children->link();
                  }
                @endphp
                <li class="header__submenu--child">
                  @if(url()->current() != env('APP_URL').$url_all_child && !empty($url_all_child))
                    <a href="{{$url_all_child}}"
                       class="header__submenu--link">{{$menu_children->getTranslatedAttribute('title')}}</a>
                  @else
                    <span class="header__submenu--link @if(url()->current() === env('APP_URL').$url_all_child) active @endif">
                    {{$menu_children->getTranslatedAttribute('title')}}
                  </span>
                  @endif
                </li>
              @endforeach
            </ul>
          @endif
        </li>
      @endforeach
    </ul>
  </nav>
@endif
