@if(isset($menu_header) && !empty($menu_header))
	@foreach($menu_header as $menu_item)
		<ul>
			@php
				if (!empty($menu_item->url)){
						if ($language === 'en'){
								$url_all = $menu_item->url;
						} else {
								$url_all = '/'.$language.$menu_item->url;
						}
				} else {
						$url_all =  $menu_item->link();
				}
			@endphp
			<li>
				@if(url()->current() != env('APP_URL').$url_all && !empty($url_all))
					<a href="{{$url_all}}">{{ $menu_item->getTranslatedAttribute('title') }}</a>
				@else
					<span @if(url()->current() === env('APP_URL').$url_all) class="active" @endif>{{ $menu_item->getTranslatedAttribute('title') }}</span>
				@endif
			</li>
			@if($menu_item->children->count())
				@foreach($menu_item->children as $menu_children)
					@php
						if (!empty($menu_children->url)){
								if ($language === 'en'){
										$url_all_child = $menu_children->url;
								} else {
										$url_all_child = '/'.$language.$menu_children->url;
								}
						} else {
								$url_all_child =  $menu_children->link();
						}
					@endphp
					<li>
						@if(url()->current() != env('APP_URL').$url_all_child && !empty($url_all_child))
							<a href="{{$url_all_child}}">{{$menu_children->getTranslatedAttribute('title')}}</a>
						@else
							<span @if(url()->current() === env('APP_URL').$url_all_child) class="active" @endif>{{$menu_children->getTranslatedAttribute('title')}}</span>
						@endif
					</li>
				@endforeach
			@endif
		</ul>
	@endforeach
@endif