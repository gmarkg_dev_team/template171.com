<ul class="footer__payments">
	<li class="mastercard"><img src="{{asset('/images/mastercard-logo.svg')}}" width="50" height="38" alt="mastercard"></li>
	<li class="visa"><img src="{{asset('/images/visa-logo.svg')}}" width="70" height="22" alt="visa"></li>
</ul>
