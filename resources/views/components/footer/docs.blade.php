<ul>
	@if(isset($docs) && !empty($docs))
		@foreach($docs as $doc)
			@php
				if ($language === 'en') {
					$url_doc = '/documents/'.$doc->slug;
				} else {
					$url_doc = '/'.$language.'/documents/'.$doc->slug;
				}
			@endphp
			<li><a target='_blank'
						 href="{{$url_doc}}">{{ !empty($doc->getTranslatedAttribute('title')) ? $doc->getTranslatedAttribute('title') : '' }}</a>
			</li>
		@endforeach
	@elseif(!$documents->isEmpty())
		@foreach($documents as $document)
			@if(json_decode($document->file))
				@foreach(json_decode($document->file) as $file)
					<li><a target='_blank'
								 href="/storage/{{$file->download_link}}">{{ !empty($document->getTranslatedAttribute('title')) ? $document->getTranslatedAttribute('title') : '' }}</a>
					</li>
				@endforeach
			@endif
		@endforeach
	@else
		<li><a href="javascript:">{{__('footer.docs.privacy_policy')}}</a></li>
		<li><a href="javascript:">{{__('footer.docs.terms_and_conditions')}}</a></li>
		<li><a href="javascript:">{{__('footer.docs.privacy_policy')}}</a></li>
		<li><a href="javascript:">{{__('footer.docs.anti_money_laundering')}}</a></li>
	@endif
</ul>