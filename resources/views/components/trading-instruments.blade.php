@if(isset($pages) && is_array($pages))
  @foreach($pages as $item)
    <a href="/{{ ($language === 'en') ? $item->slug : $language.'/'.$item->slug }}">
      <div style="background-image: url('/storage/{{ $item->image }}');"></div>
      @if(!empty($item->getTranslatedAttribute('title')))
        <span>{{ $item->getTranslatedAttribute('title') }}</span>
      @endif
    </a>
  @endforeach
@endif