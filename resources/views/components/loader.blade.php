{{-- Don't use 'rem' --}}

@push('loader-css')
  <style>
    *{box-sizing: border-box}body{overflow: hidden}
		.loader-wrap {
			position: fixed;
			top: 0;
			left: 0;
			right: 0;
			bottom: 0;
			background-color: #ccc;
			z-index: 100;
		}
  </style>
@endpush

@push('loader-wrap')
	<div class="loader-wrap"></div>
@endpush