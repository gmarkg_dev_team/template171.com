@php($reviewsTitle = __('engine.home.reviews.title'))
<div class="reviews">
  <div class="reviews__box box">
    <article class="reviews__content">
      @if(!empty($reviewsTitle) && $reviewsTitle !== 'engine.home.reviews.title')
        <h3 class="reviews__title title">{!! $reviewsTitle !!}</h3>
      @endif
    </article>
  </div>
  <div id="js-reviews-slider" class="reviews__slider box">
    <div class="js-slider reviews__slider-row">
      @if(isset($reviews) && !$reviews->isEmpty())
        @foreach($reviews as $slide)
          <div class="reviews__slider-column">
            <div class="reviews__item">
              <div class="reviews__item-profile">
                <div class="reviews__item-image" style="background-image: url('{{$slide->imageUrl()}}');"></div>
                <p class="reviews__item-author">{!! $slide->getTranslatedAttribute('author') !!}</p>
                <p class="reviews__item-position">{!! $slide->getTranslatedAttribute('position') !!}</p>
              </div>
              <p class="reviews__item-text">{!! $slide->getTranslatedAttribute('text') !!}</p>
            </div>
          </div>
        @endforeach
      @elseif(env('APP_ENV') !== 'production')
        @for ($i = 0; $i < 8; $i++)
          <div class="reviews__slider-column">
            <div class="reviews__item">
              <div class="reviews__item-profile">
                <div class="reviews__item-image"></div>
                <p class="reviews__item-author">Author</p>
                <p class="reviews__item-position">Position</p>
              </div>
              <p class="reviews__item-text">Lorem ipsum dolor sit amet, consectetur adipisicing.</p>
            </div>
          </div>
        @endfor
      @endif
    </div>
  </div>
</div>