<div class="news__list">
  @php($page = !empty($page) ? $page : '')
  @if(!$variable->isEmpty())
    @foreach($variable as $item)
      <div class="news__item">
        <div class="news__img" style="background-image: url({{$item->imageUrl()}});"></div>
        <section class="news__text">
          @php($date = $item->getOrderColumn())
          @php($time = \Carbon\Carbon::parse($item->$date))
          <time class="news__text--time">{{$time->format('d.m.Y')}}</time>
          <h3 class="news__text--title">
            <a class="news__text--a"
               href="{{route($name.'.one',['page'=>$item->getTranslatedAttribute('slug')])}}">{{$item->getTranslatedAttribute('title')}}</a>
          </h3>
          <p class="news__text--excerpt">{{$item->getTranslatedAttribute('excerpt')}}</p>
          <a class="news__text--more"
             href="{{route($name.'.one',['page'=>$item->getTranslatedAttribute('slug')])}}">{{__('engine.button.read more')}}</a>
        </section>
      </div>
    @endforeach
    @if((count($variable) % config('engine.pagination.news.home')) != 0 && $page !== 'home')
      <div class="news__item last"></div>
    @endif
  @elseif(env('APP_ENV') !== 'production')
    @for ($i = 0; $i < config('engine.pagination.news.home'); $i++)
      <div class="news__item">
        <div class="news__img" style="background-color: grey"></div>
        <section class="news__text">
          <time class="news__text--time">23.10.2019</time>
          <h3 class="news__text--title">
            <a class="news__text--a" href="#!">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error,harum.
            </a>
          </h3>
          <p class="news__text--excerpt">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam doloribus optio quae reprehenderit tempora
            voluptas!
          </p>
          <a class="news__text--more" href="#!">{{__('engine.button.read more')}}</a>
        </section>
      </div>
    @endfor
  @endif
</div>