<footer class="footer" itemscope="itemscope" itemtype="https://schema.org/WPFooter">
  <div class="box footer__box">
    <div class="footer__left">
      <figure class="footer__logo">
        <img src="{{asset('images/logo.svg')}}" alt="{{setting('requisites.company-name')}}" title="{{setting('requisites.company-name')}}">
      </figure>
      <div class="footer__contact">
				@if(!empty(setting('site.phone-number')))
        <div class="footer__tel">
					<a href="tel:{{ setting('site.phone-number') }}">{{ setting('site.phone-number') }}</a>
        </div>
				@endif
				@if(!empty(setting('site.support-email')))
        <div class="footer__email">
					<a href="mailto:{{ setting('site.support-email') }}">{{ setting('site.support-email') }}</a>
        </div>
					@endif
        @if(!empty(setting('site.adress')))
          <p class="footer__addr">{{setting('site.adress')}}</p>
        @endif
      </div>
			@include('components.footer.payments')
    </div>
		<div class="footer__right">
			<nav class="footer__menu">
				@include('components.footer.menu')
			</nav>
			<nav class="footer__docs">
				@include('components.footer.docs')
			</nav>
			<div class="footer__terms">
				<div class="footer__terms-column">
					<h4>{!! __('footer.risk') !!}</h4>
					<p>{!! __('footer.risk_text') !!}</p>
				</div>
				<div class="footer__terms-column">
					<h4>{!! __('footer.legal') !!}</h4>
					<p>{!! __('footer.legal_text') !!}</p>
				</div>
			</div>
			<p class="footer__copy">{!! __('footer.copy') !!}</p>
		</div>
  </div>
</footer>
