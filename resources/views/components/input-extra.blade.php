{{-- Component for additional form inputs --}}
{{-- Use field parameter to add component to form --}}

<div class="form__row">
  <label for="{{$prefix}}_subject" class="form__label">@lang('form.labels.subject')</label>
  <input class="form__input" id="{{$prefix}}_subject"  name="subject" type="text" placeholder="@lang('form.placeholders.subject')" required autocomplete="off" inputmode="text">
</div>
<div class="form__row">
  <label for="{{$prefix}}_message" class="form__label">@lang('form.labels.message')</label>
  <textarea class="form__input textarea" rows="5" id="{{$prefix}}_message" name="message" placeholder="@lang('form.placeholders.message')" required autocomplete="off" inputmode="text"></textarea>
</div>