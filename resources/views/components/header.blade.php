@include('engine::components.lang', ['iconPrefix' => 'flag-icon'])
<header class="header" itemscope itemtype="https://schema.org/WPHeader">
  <div class="box header__box">

    <a class="header__logo header__logo--mobile" href="{{route('home')}}">
      <img src="{{ asset('images/logo.svg') }}" alt="{{setting('requisites.company-name')}}"
           title="{{setting('requisites.company-name')}}">
    </a>

    <div class="header__menu js-nav">
      <a class="header__logo" href="{{route('home')}}">
        <img src="{{ asset('images/logo.svg') }}" alt="{{setting('requisites.company-name')}}"
             title="{{setting('requisites.company-name')}}">
      </a>

      @if(isset($menu_header))
        @include('components.menu', ['items' => $menu_header])
      @endif

      <div class="header__right">
        <div class="header__enter">
          <a href="{{ route('login') }}" class="header__login">{{ __('header.button.login') }}</a>
          <a href="{{ route('live-register') }}" class="header__register">{{ __('header.button.registration') }}</a>
        </div>
        @include('components.lang')
      </div>

    </div>

    <div class="header__burger js-burger">
      <div></div>
    </div>

  </div>
</header>
