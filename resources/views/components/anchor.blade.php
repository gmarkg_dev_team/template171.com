<a href="#wrapper" class="page-anchor">
	<svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18" fill="none">
		<path d="M9 16.2856L9 1.71422" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
		<path d="M1.71436 9L9.00007 1.71429L16.2858 9" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
	</svg>
</a>