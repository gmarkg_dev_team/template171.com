<nav class="breadcrumbs box">
  <ul class="breadcrumbs__list" itemscope="" itemtype="http://schema.org/BreadcrumbList">
    <li class="breadcrumbs__item" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
      <a class="breadcrumbs__a" itemprop="item" href="{{route('home')}}"><span
          itemprop="name">{{__('general.breadcrumbs.home')}}</span></a>
    </li>
    @if(!$parent)
      <li class="breadcrumbs__item" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
        <a class="breadcrumbs__a" itemprop="item" href="{{$route}}"><span itemprop="name">{{$titlePar}}</span></a>
      </li>
      <li class="breadcrumbs__item" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
        <a class="breadcrumbs__a go-to" itemprop="item" href="#h1"><span itemprop="name">{{$title}}</span></a>
      </li>
    @else
      <li class="breadcrumbs__item" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
        <a class="breadcrumbs__a go-to" itemprop="item" href="#h1"><span itemprop="name">{{$title}}</span></a>
      </li>
    @endif
  </ul>
</nav>