<div class="header__lang">
	<div class="header__lang--current">
		@stack('current_lang')
	</div>
	<ul class="header__lang--list">
		@stack('switcher')
	</ul>
</div>