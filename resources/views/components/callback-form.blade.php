<form class="form-callback" action="">
	{{ csrf_field() }}
	<div class="form__row">
		<div class="form__input form__input--callback callback-select">{{__('form.placeholders.callback')}}</div>
	</div>
	<div class="form__row terms">
		<input id="callback_check" type="checkbox" name="check" autocomplete="off">
		<label for="callback_check" class="form__check">
				<span class="form__terms">
						{!! __('form.agree') !!} <a href="{{setting('site.terms-conditions')}}" target="_blank">{!! __('form.terms') !!}</a>
				</span>
		</label>
	</div>
	<div class="form__row submit">
		<button class="form__btn" type="submit">{{__('engine.button.send')}}</button>
	</div>
	<p class="form-callback__success success" data-success="{{__('form.ajax.success')}}"></p>
	<p class="form-callback__error error" data-err="{{__('form.ajax.error')}}" data-field="{{__('errors.field') }}"></p>
</form>