@if ($paginator->lastPage() > 1)
  <nav class="pagination">
    <a class="pagination__prev pagination__a{{ $paginator->currentPage() == 1 ? ' disabled' : '' }}"
       href="{{$paginator->currentPage() == 1 ? 'javascript:' : $paginator->url(1)}}"><span>{{__('engine.button.prev')}}</span></a>
    @for ($i = 1; $i <= $paginator->lastPage(); $i++)
      <a class="pagination__a{{ ($paginator->currentPage() == $i) ? ' active' : '' }}"
         href="{{ $paginator->url($i) }}">{{ $i }}</a>
    @endfor
    <a
      class="pagination__next pagination__a{{ ($paginator->currentPage() == $paginator->lastPage()) ? ' disabled' : '' }}"
      href="{{$paginator->currentPage() == $paginator->lastPage() ? 'javascript:' : $paginator->url($paginator->currentPage()+1)}}"><span>{{__('engine.button.next')}}</span></a>
  </nav>
@endif