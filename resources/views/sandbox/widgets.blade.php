@component('layouts.index')
	@slot('pageCss','widgets')
	@slot('pageJs','widgets')

	<main class="site-content page-widgets">

    @php($corpQuotes = [
			'uber'      => ['name' => 'UBER', 'title' => 'Uber Technologies Inc'],
			'apple'     => ['name' => 'APPL', 'title' => 'Apple In'],
			'intel'     => ['name' => 'INTC', 'title' => 'Intel Corporation'],
			'amazon'    => ['name' => 'AMZN', 'title' => 'Amazon.com Inc'],
			'tesla'     => ['name' => 'TSL', 'title' => 'Tesla Inc'],
			'netflix'   => ['name' => 'NFLX', 'title' => 'Netflix Inc'],
			'microsoft' => ['name' => 'MSFT', 'title' => 'Microsoft Corporation'],
			'facebook'  => ['name' => 'FB',   'title' => 'Facebook Inc'],
			'dropbox'   => ['name' => 'DBX',  'title' => 'Dropbox Inc'],
		])
    <section class="corp-quotes">
      <div class="corp-quotes__box box">
        @if(!empty($corpQuotes) && is_array($corpQuotes))
          <div class="js-corp-quotes corp-quotes__widget">

            <div class="corp-quotes__row js-slider">
              @foreach($corpQuotes as $key => $item)
                <div class="corp-quotes__column">
                  {{--item--}}
                  <div class="corp-quotes__item">
                    {{--header--}}
                    <div class="corp-quotes__item-header">
                      @if(isset($item['title']) && !empty($item['title']))
                        <div class="corp-quotes__item-title">{!! $item['title'] !!}</div>
                      @endif
                    </div>
                    {{--header--}}
                    {{--body--}}
                    <div class="corp-quotes__item-body">
                      <div class="corp-quotes__item-body--column column--left">
                        <div class="corp-quotes__item-company">
                          <div class="corp-quotes__item-image">
                            <img src="{{ asset('/images/company-icons/'.$key.'-logo.svg') }}" alt="{{ $key }}">
                          </div>
                          @if(isset($item['name']) && !empty($item['name']))
                            <div class="corp-quotes__item-name">{!! $item['name'] !!}</div>
                          @endif
                        </div>
                      </div>
                      <div class="corp-quotes__item-body--column column--right">
                        <div class="js-{{ $key }} corp-quotes__item-statistics"></div>
                      </div>
                    </div>
                    {{--body--}}
                  </div>
                  {{--item--}}
                </div>
              @endforeach
            </div>

          </div>
        @endif
      </div>
    </section>

		@php($cryptoQuotes = [
			'btcusd' => [
				'title'         => 'Bitcoin',
				'commonOptions' => [
					'className' => 'bitcoin',
					'updateDelay' => 4, //тротлинг для обновления данных в секундах
				],
				'chartOptions'  => [
					'borderColor'          => '#f7931a',
					'pointBackgroundColor' => '#f7931a',
					'pointBorderColor'     => 'rgba(247,147,26,0.2)',
				],
			],
			'xrpusd' => [
				'title'         => 'XRP',
				'commonOptions' => [
					'className' => 'xrp',
					'digits'    => 5, //количество знаков после точки для цены
				],
				'chartOptions'  => [
					'borderColor'          => '#7e7e7e',
					'pointBackgroundColor' => '#7e7e7e',
					'pointBorderColor'     => 'rgba(126,126,126,0.2)',
				],
			],
			'ltcusd' => [
				'title'         => 'Litecoin',
				'commonOptions' => [
					'className' => 'litecoin',
				],
				'chartOptions'  => [
					'borderColor'          => '#bfbbbb',
					'pointBackgroundColor' => '#bfbbbb',
					'pointBorderColor'     => 'rgba(191,187,187,0.2)',
				],
			],
			'ethusd' => [
				'title'         => 'Ethereum Classic',
				'commonOptions' => [
					'className' => 'ether-classic',
				],
				'chartOptions'  => [
					'borderColor'          => '#669073',
					'pointBackgroundColor' => '#669073',
					'pointBorderColor'     => 'rgba(102,144,115,0.2)',
				],
			],
			'etcusd' => [
				'title'         => 'Ethereum',
				'commonOptions' => [
					'className' => 'ether-default',
				],
				'chartOptions'  => [
					'borderColor'          => 'rgba(1,1,1,0.6)',
					'pointBackgroundColor' => 'rgba(1,1,1,0.6)',
					'pointBorderColor'     => 'rgba(1,1,1,0.2)',
				],
			],
		])
		<div class="crypto-quotes">
			<div class="crypto-quotes__box box">
				@if(isset($cryptoQuotes) && is_array($cryptoQuotes))
					<div class="js-crypto-slider crypto-quotes__slider">
						@foreach($cryptoQuotes as $key => $item)
							<div data-key="{{$key}}"
									 data-common-options="{{ isset($item['commonOptions']) ? json_encode($item['commonOptions']) : '' }}"
									 data-chart-options="{{ isset($item['chartOptions']) ? json_encode($item['chartOptions']) : '' }}"
									 class="js-crypto-widget crypto-widget">
								<div class="js-{{$key}} crypto-widget__inside">
									<div class="crypto-widget__header">
										<div class="crypto-widget__header-inside">
											<i class="crypto-widget__header-icon"></i>
											@if(isset($item['title']))
												<div class="crypto-widget__title">{{$item['title']}}</div>
											@endif
											<div class="crypto-widget__date">
												<div class="js-date-watch crypto-widget__date-watch active"></div>
												<div class="js-date-chart crypto-widget__date-chart"></div>
											</div>
										</div>
									</div>
									<div class="crypto-widget__body">
										<div class="crypto-widget__price">
											<div class="js-price-watch crypto-widget__price-watch active">
												<p>$<span class="js-price"></span></p>
											</div>
											<div class="js-price-chart crypto-widget__price-chart">
												<p>$<span class="js-price"></span></p>
											</div>
											<div data-after="24h"
													 class="js-price-delta crypto-widget__price-delta">
												<p>$<span class="js-static"></span>(<span class="js-percent"></span>%)</p>
											</div>
										</div>
									</div>
									<div class="crypto-widget__canvas">
										<canvas class="js-canvas"></canvas>
									</div>
								</div>
							</div>
						@endforeach
					</div>
				@endif
			</div>
		</div>

		@php($sliderQuotes = [
			'eurusd' => ['title' => 'EUR/USD'],
			'btcusd' => ['title' => 'BTC/USD'],
			'ethusd' => ['title' => 'ETH/USD'],
			'gbpusd' => ['title' => 'GBP/USD'],
		])
		<div class="slider-quotes js-slider-quotes">
			<div class="slider-quotes__box box">
				@if(!empty($sliderQuotes) && is_array($sliderQuotes))
				<div class="slider-quotes__main js-slider">
					@foreach($sliderQuotes as $key => $value)
						<div>
							<div class="slider-quotes__item">
								<div class="slider-quotes__item--content">
									@if(isset($value['title']) && !empty($value['title']))
										<div class="quotes__item--title">{!! $value['title'] !!}</div>
									@endif
									<div class="slider-quotes__item--value js-{{$key}}"></div>
									<div class="slider-quotes__item--icon">
										<div>
											<svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
												<path d="M9 1.71429L9 16.2857" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
												<path d="M1.71436 9L9.00007 16.2857L16.2858 9" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
											</svg>
										</div>
									</div>
								</div>
							</div>
						</div>
					@endforeach
				</div>
				@endif
				<div class="slider-quotes__sidebar">
					<p>{!! __('widgets.slider-quotes.text') !!}</p>
				</div>
			</div>
		</div>

		@php($tickerQuotes = [
			'eurusd' => ['title' => 'EUR/USD'],
			'gbpusd' => ['title' => 'GBP/USD'],
			'usdcnh' => ['title' => 'USD/CNH'],
			'nas100' => ['title' => 'Nasdaq 100'],
			'btcusd' => ['title' => 'BTC/USD'],
			'ethusd' => ['title' => 'ETH/USD'],
			'ltcusd' => ['title' => 'LTC/USD'],
			'spot' => ['title' => 'Spotify'],
		])
		<div class="ticker-quotes js-ticker-quotes">
			@if(isset($tickerQuotes) && is_array($tickerQuotes))
				<ul class="ticker-quotes__list">
					@for($tickerQuotesIndex = 0; $tickerQuotesIndex < 5; $tickerQuotesIndex+=1)
						@foreach($tickerQuotes as $key => $item)
							<li class="ticker-quotes__item">
								@if(isset($item['title']))
									<span class="ticker-quotes__item--key">{{$item['title']}}</span>
								@endif
								<div class="js-{{$key}}">
									<span class="ticker-quotes__item--bid-ask"></span>
								</div>
							</li>
						@endforeach
					@endfor
				</ul>
			@endif
		</div>

		@php($chartQuotes = [
			'btcusd' => ['title' => 'BTC/USD'],
			'eurusd' => ['title' => 'EUR/USD'],
			'nas100' => ['title' => 'Nasdaq 100'],
			'ethusd' => ['title' => 'ETH/USD'],
		])
		<div class="box">
			<div data-autoplay
					 data-start-index="0"
					 data-autoplay-speed="5000"
					 class="chart-quotes js-chart-quotes">
				@if(isset($chartQuotes) && is_array($chartQuotes))
					<div class="chart-quotes__header">
						<canvas class="js-canvas"></canvas>
					</div>
					<div class="chart-quotes__body">
						<ul class="chart-quotes__list">
							@foreach($chartQuotes as $key => $item)
								<li>
									<div data-key="{{$key}}" class="js-item chart-quotes__item">
										<div class="chart-quotes__item--left">
											@if(isset($item['title']))
												<div class="chart-quotes__item--title">{{$item['title']}}</div>
											@endif
										</div>
										<div class="chart-quotes__item--right js-{{$key}}"></div>
									</div>
								</li>
							@endforeach
						</ul>
					</div>
				@endif
			</div>
		</div>

		@php($tableQuotes = [
			'eurusd','gbpusd','usdchf','usdcad','usdcnh','usdsek','eurgbp'
		])
		<div class="box">
			<table class="table-quotes js-table-quotes">
				<tr>
					<th></th>
					<th>Bid</th>
					<th>Ask</th>
				</tr>
				@if(isset($tableQuotes) && is_array($tableQuotes))
					@foreach($tableQuotes as $item)
						<tr class="js-{{$item}}"></tr>
					@endforeach
				@endif
			</table>
		</div>

		@php($tradingQuotes = [
			'eurusd' => ['title' => 'EUR/USD'],
			'eurcad' => ['title' => 'EUR/CAD'],
			'gbpusd' => ['title' => 'GBP/USD'],
			'usdsek' => ['title' => 'USD/SEK'],
			'gbpcad' => ['title' => 'GBP/CAD'],
		])
		<div class="trading-quotes js-trading-quotes"
				 data-autoplay
				 data-start-index="0"
				 data-autoplay-speed="5000"
				 data-item-price-title="{{__('widgets.trading-quotes.price-title')}}"
				 data-item-percent-title="{{__('widgets.trading-quotes.percent-title')}}">
			@if(!empty($tradingQuotes) && is_array($tradingQuotes))
			<div class="trading-quotes__box box">
				<div class="trading-quotes__header">
					<ul>
						@foreach($tradingQuotes as $key => $item)
							<li data-key="{{$key}}"
									class="js-tab-item">
								<span class="trading-quotes__header--icon {{$key}}"></span>
								@if(isset($item['title']))
									<span class="trading-quotes__header--title">{{$item['title']}}</span>
								@endif
							</li>
						@endforeach
					</ul>
				</div>
				<div class="trading-quotes__body">
					<div class="trading-quotes__body--content">
						@foreach($tradingQuotes as $key => $item)
							<div data-key="{{$key}}"
									 class="trading-quotes__item js-{{$key}} js-currency-item"></div>
						@endforeach
						<a href="{{ route('live-register') }}" class="trading-quotes__item--btn btn">{{ __('engine.button.start') }}</a>
					</div>
					<div class="trading-quotes__chart">
						<div class="trading-quotes__chart--canvas">
							<canvas class="js-canvas"></canvas>
						</div>
					</div>
				</div>
			</div>
			@endif
		</div>

		@php($categoriesQuotesCols = __('widgets.categories-quotes.cols'))
		@php($categoriesQuotes = [
			'forex'       => [
				'btcusd' => ['title' => 'BTC/USD'],
				'eurusd' => ['title' => 'EUR/USD'],
				'gbpusd' => ['title' => 'GBP/USD'],
				'usdcnh' => ['title' => 'USD/CNH']
			],
			'shares'      => [
				'google'  => ['title' => 'Google'],
				'appl'   => ['title' => 'Apple'],
				'alibaba' => ['title' => 'Alibaba'],
				'tsl'   => ['title' => 'Tesla']
			],
			'commodities' => [
				'coffee' => ['title' => 'Coffee'],
				'sugar'  => ['title' => 'Sugar'],
				'wheat'  => ['title' => 'Wheat'],
				'cotton' => ['title' => 'Cotton']
			],
			'indices'     => [
				'jpn225' => ['title' => 'JPN225'],
				'us30'   => ['title' => 'Dow Jones'],
				'nas100' => ['title' => 'NASDAQ'],
				'ita40'  => ['title' => 'ITA40']
			],
		])
		<div data-autoplay
				 data-start-index="0"
				 data-autoplay-speed="3000"
				 class="categories-quotes js-categories-quotes">
			@if(isset($categoriesQuotes) && is_array($categoriesQuotes))
				<div class="categories-quotes__box box">
					<div class="categories-quotes__nav">
						@foreach($categoriesQuotes as $key => $category)
							<button data-key="{{ $key }}" class="categories-quotes__btn js-btn">
								<span class="js-{{$key}}"></span>
								{{ __('widgets.categories-quotes.navs.'.$key) }}
							</button>
						@endforeach
					</div>
					<div class="categories-quotes__body">
						@foreach($categoriesQuotes as $key => $category)
							<div data-key="{{$key}}" class="categories-quotes__tab js-tab">
								<div class="categories-quotes__row">
									@if(isset($categoriesQuotesCols) && is_array($categoriesQuotesCols))
										@foreach($categoriesQuotesCols as $key => $item)
											<div class="categories-quotes__cell">{{ $item }}</div>
										@endforeach
									@endif
								</div>
								@foreach($category as $key => $item)
									<div class="categories-quotes__row">
										@if(isset($item['title']))
											<div class="categories-quotes__cell cell--static">{{$item['title']}}</div>
											<div class="categories-quotes__cell cell--dynamic js-{{$key}}"></div>
										@endif
									</div>
								@endforeach
							</div>
						@endforeach
					</div>
				</div>
			@endif
		</div>

	</main>

	@push('slick')
		<script {{ env('EFFICIENTLY_LOAD') ? 'defer' : ''}} src="{{ route('js', ['filename' => 'slick.min']) }}"></script>
		<script {{ env('EFFICIENTLY_LOAD') ? 'defer' : ''}} src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
	@endpush
@endcomponent
