@component('layouts.index')
  @php($title = __('page_faq.title'))

  @slot('title',$title.' - '.setting('site.title'))
  @slot('pageCss','faq')
  @slot('pageJs','page')

  <main class="site-content pt page-faq" data-room="{{$socketRoom}}">
    @include("components.bread",['title' => $title,'parent' => true])
    <article class="box faq content-padding">
      <h1 id="h1" class="title">{!! $title !!}</h1>

      @if (!empty($faq))
        <ol>
          @foreach ($faq as $item)
            <li>
              <a
                href="{{route('faq.one',['page'=>$item->getTranslatedAttribute('slug')])}}">{{$item->getTranslatedAttribute('question')}}</a>
            </li>
          @endforeach
        </ol>
      @endif
    </article>
  </main>

@endcomponent