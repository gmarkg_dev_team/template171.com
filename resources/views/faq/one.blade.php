@component('layouts.index')
  @slot('title',$faq->getTranslatedAttribute('question').' - '.setting('site.title'))
  @slot('pageCss','page')
  @slot('pageJs','page')

  <main class="site-content pt page-faq__one" data-room="{{$socketRoom}}">
    @include("components.bread",['title' => $faq->getTranslatedAttribute('question'),'titlePar' => __('page_faq.title'),'route' => route('faq'),'parent' => false])
    <article class="box gui content-padding">
      <h1 id="h1">{{$faq->getTranslatedAttribute('question') }}</h1>
      {!! $faq->getTranslatedAttribute('answer') !!}
      <div style="clear: both"></div>
    </article>
  </main>

@endcomponent