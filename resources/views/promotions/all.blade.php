@component('layouts.index')
  @php($title = __('page_promotions.title'))

  @slot('title', $title.' - '.setting('site.title'))
  @slot('pageCss','news')
  @slot('pageJs','page')
  @slot('meta')
    @php($rel_url=$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME'].explode('?',$_SERVER['REQUEST_URI'])[0])
    @if(isset($_GET['page']) && $_GET['page'] == '2')
      <link rel="next" href="{{$rel_url}}?page=3">
      <link rel="prev" href="{{$rel_url}}">
    @elseif(isset($_GET['page']))
      <link rel="next" href="{{$rel_url}}?page={{$_GET['page'] + 1}}">
      <link rel="prev" href="{{$rel_url}}?page={{$_GET['page'] - 1}}">
    @else
      <link rel="next" href="{{$rel_url}}?page=2">
    @endif
  @endslot

  <main class="site-content pt page-news" data-room="{{$socketRoom}}">
    @include("components.bread", ['title' => $title, 'parent' => true])
    <article class="box news content-padding">
      <h1 id="h1" class="news__title title">{!! $title !!}</h1>
      @include('components.news',['name' => 'promotions', 'variable' => $promotions])
      {{ $promotions->render('components.pagination') }}
    </article>
  </main>

@endcomponent