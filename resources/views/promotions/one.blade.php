@component('layouts.index')
  @slot('title',!empty($promotion->getTranslatedAttribute('seo_title')) ? $promotion->getTranslatedAttribute('seo_title') : $promotion->getTranslatedAttribute('title').' - '.setting('site.title'))
  @slot('description', $promotion->getTranslatedAttribute('seo_description'))
  @slot('pageCss','page')
  @slot('pageJs','page')

  <main class="site-content pt page-news__one" data-room="{{$socketRoom}}">
    @include("components.bread",['title' => $promotion->getTranslatedAttribute('title'),'titlePar' => __('page_promotions.title'), 'route' => route('promotions'),'parent' => false])
    <article class="box gui content-padding">
      <h1 id="h1">{{ $promotion->getTranslatedAttribute('title') }}</h1>
      <div class="gui__date">
        @php($date = $promotion->getOrderColumn())
        @php($time = \Carbon\Carbon::parse($promotion->$date))
        <time>{{$time->format('d.m.Y')}}</time>
      </div>
      <figure class="gui__img">
        <img src="{{$promotion->imageUrl()}}" alt="{{$promotion->getTranslatedAttribute('image_alt')}}">
      </figure>
      {!!$promotion->getTranslatedAttribute('content')!!}
      <div style="clear: both"></div>
    </article>
  </main>

@endcomponent