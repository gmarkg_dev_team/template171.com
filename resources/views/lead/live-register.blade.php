@component('layouts.index')
  @php($title = __('page_registration.title'))

  @slot('title', $title.' - '.setting('site.title'))
  @slot('pageCss','lead')
  @slot('pageJs','lead')

  <main class="site-content pt page-lead" data-room="{{$socketRoom}}">
    @include('components.bread', ['title' => $title,'parent' => true])
    <div class="box lead__box">
      <article class="lead__form">
        <h1 id="h1" class="lead__form--title">{!! $title !!}</h1>
        @include('engine::components.form',['prefix'=>'live','route' => route('live-register.create')])
      </article>
    </div>
  </main>

@endcomponent