const mix = require('laravel-mix');
const Dotenv = require('dotenv-webpack');

require('laravel-mix-polyfill');
require('laravel-mix-merge-manifest');
require('laravel-mix-copy-watched');

mix
  .sass('resources/assets/styles/bundles/main.scss', 'public/css/main.css')
  .js('resources/assets/js/app.js', 'public/js')
  .sourceMaps()
  .webpackConfig({
    plugins: [
      new Dotenv(),
    ],
  })
  .options({
    autoprefixer: {remove: false}
  })
  .polyfill({
    enabled: true,
    useBuiltIns: "usage",
    targets: "firefox 50, IE 11"
  })
  .copyWatched(
    'resources/assets/images/**/*.{jpg,jpeg,png,gif,svg}',
    'public/images',
    {base: 'images'}
  )
  .copyWatched(
    'resources/assets/fonts/*.{ttf,otf,eot,woff,woff2,svg}',
    'public/fonts',
  )
  .copyWatched(
    'resources/assets/favicon/*.{jpg,jpeg,png,gif,svg,json,webmanifest,ico,xml}',
    'public/favicon',
    {base: 'favicon'}
  )
  .browserSync(process.env.APP_URL)
  .mergeManifest();

if (mix.inProduction()) {
  mix
    .disableNotifications()
    .version();
}
